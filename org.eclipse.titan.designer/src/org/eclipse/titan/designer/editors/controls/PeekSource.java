/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.controls;

import java.io.RandomAccessFile;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.titan.designer.AST.Location;
import org.eclipse.titan.designer.AST.NULL_Location;
import org.eclipse.titan.designer.editors.ColorManager;
import org.eclipse.titan.designer.editors.ttcn3editor.CodeScanner;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;

/**
 * @author Miklos Magyari
 * @author Adam Knapp
 * */
public class PeekSource {
	public static String getPeekSource(IEditorPart targetEditor, Location codeLoc) {
		if (codeLoc == null || codeLoc.equals(NULL_Location.INSTANCE)) {
			return null;
		}

		final IResource fileToLoad = codeLoc.getFile();
		IResource actual = (IResource)targetEditor.getEditorInput().getAdapter(IFile.class);

		String codeText = null;
		if (targetEditor instanceof ITextEditor) {
			ITextEditor textEditor = (ITextEditor) targetEditor;
			IDocumentProvider provider = textEditor.getDocumentProvider();
			IEditorInput input = targetEditor.getEditorInput();
			IDocument document;
			if (actual.getFullPath().equals(fileToLoad.getFullPath())) {
				document = provider.getDocument(input);
				StringBuilder code = new StringBuilder();
				try {
					for (int i = codeLoc.getOffset(); i <= codeLoc.getEndOffset(); i++) {
						char c = document.getChar(i);
						code.append(c);
					}
				} catch (Exception e) {
					return null;
				}
				codeText = code.toString();
			} else {
				// external file
				try {
					RandomAccessFile f2 = new RandomAccessFile(fileToLoad.getLocation().toString(), "r");
					final int size = codeLoc.getEndOffset() - codeLoc.getOffset();
					byte[] buffer = new byte[size];
					f2.seek(codeLoc.getOffset());
					f2.read(buffer, 0, size);
					codeText = new String(buffer);
				} catch (Exception e) {
					return null;
				}
			} 				

			return codeText;
		}
		return null;
	}

	public static void addStyledSource(String source, Ttcn3HoverContent content) {
		if (source == null || source.isEmpty()) {
			content.addContent(HoverContentType.SOURCE, "");
			return;
		}

		/*
		 * We may need to strip some leading tabs/spaces from all lines except the first one
		 * 
		 * This is because whitespaces are typically missing for the first line but all the other
		 * lines are indented
		 */
		String[] lines = source.split("\n");
		int minspaces = Integer.MAX_VALUE;
		int spacecount = 0;
		if (lines.length > 1) {
			StringBuilder notabs = new StringBuilder();
			for (int i = 1; i < lines.length; i++) {
				spacecount = 0;
				for (int j = 0; j < lines[i].length(); j++) {
					if (lines[i].charAt(j) == '\t') {
						spacecount += 4;
						notabs.append("    ");
					} else if (lines[i].charAt(j) == ' ') {
						spacecount++;
						notabs.append(" ");
					} else {
						notabs.append(lines[i].substring(j));
						break;
					}
				}
				notabs.append("\n");
				if (spacecount < minspaces) {
					minspaces = spacecount;
				}
			}		
			StringBuilder stripped = new StringBuilder();
			if (minspaces != Integer.MAX_VALUE && minspaces > 0) {
				stripped.append(lines[0]);
				lines = notabs.toString().split("\n");
				for (int i = 0; i < lines.length; i++) {
					stripped.append("\n");
					stripped.append(lines[i].substring(minspaces));
				}
				source = stripped.toString();
			}
		}
		
		IDocument doc = new Document(source);
		CodeScanner scanner = new CodeScanner(new ColorManager());
		scanner.setRange(doc, 0, source.length());
		IToken token;
		StringBuilder codestr = new StringBuilder("<pre>");
		for (;;) {
			token = scanner.nextToken();
			if (token.isEOF())
				break;

			boolean isBold = false;
			boolean isItalic = false;
			String item = source.substring(scanner.getTokenOffset(), scanner.getTokenOffset() + scanner.getTokenLength());
			item.replaceAll("\n", "<br>").replaceAll("\t", "  ");
			if (token.getData() instanceof TextAttribute) {
				TextAttribute attrib = (TextAttribute)token.getData();
				switch (attrib.getStyle()) {
				case SWT.BOLD:
					isBold = true;
					break;
				case SWT.ITALIC:
					isItalic = true;
					break;
				}
				codestr.append("<span style=\"");
				if (isBold) {
					codestr.append("font-weight: bold; ");
				}
				if (isItalic) {
					codestr.append("font-style: italic; ");
				}
				Color color = attrib.getForeground();
				
				final String colorcode = "rgb(" + color.getRed() + ", " + color.getGreen() + ", " + color.getBlue() + ")";
				codestr.append("color : ").append(colorcode).append("; >");
				codestr.append(" \">").append(item);
				codestr.append("</span>");
			} else {
				codestr.append(item);
			}
		}

		content.addContent(HoverContentType.SOURCE, codestr.toString());
	}
}
