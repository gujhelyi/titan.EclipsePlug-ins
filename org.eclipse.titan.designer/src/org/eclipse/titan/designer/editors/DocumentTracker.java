/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.text.IDocument;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.parsers.GlobalParser;
import org.eclipse.titan.designer.parsers.ProjectSourceParser;
import org.eclipse.ui.texteditor.AbstractDecoratedTextEditor;

/**
 * This fully static class is used to track which files are open in a document at
 * a given time.
 * <p>
 * We assume that one file can be open in one document only at a time.
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 * */
public final class DocumentTracker {

	private static final Map<IFile, IDocument> FILE_DOCUMENT_MAP = new ConcurrentHashMap<IFile, IDocument>();
	private static final Map<IDocument, Module> DOCUMENT_TTCN3MODULE_MAP = new ConcurrentHashMap<>();
	private static final Map<String, AbstractDecoratedTextEditor> MODULE_EDITOR = new ConcurrentHashMap<>();

	/** private constructor to disable instantiation */
	private DocumentTracker() {
		// intentionally empty
	}

	/**
	 * Stores the information that the provided file is opened in the provided document.
	 *
	 * @param file the provided file
	 * @param document the provided document
	 * */
	public static void put(final IFile file, final IDocument document, AbstractDecoratedTextEditor editor) {
		FILE_DOCUMENT_MAP.put(file, document);
		final ProjectSourceParser projectSourceParser = GlobalParser.getProjectSourceParser(file.getProject());
		final Module tempModule = projectSourceParser.containedModule(file);
		if (tempModule != null) {
			DOCUMENT_TTCN3MODULE_MAP.put(document, tempModule);
			MODULE_EDITOR.put(tempModule.getIdentifier().getDisplayName(), editor);
		}
	}

	/**
	 * Checks if the provided file is open in a document and returns the document.
	 *
	 * @param file the file to check for
	 * @return the document the file is open in, or null if none
	 * */
	public static IDocument get(final IFile file) {
		return FILE_DOCUMENT_MAP.get(file);
	}

	/**
	 * Returns the TTCN3Module according to the specified document
	 *
	 * @param document the document to check for
	 * @return the TTCN3Module to that the document is related, or null if none
	 * */
	public static Module get(final IDocument document) {
		// the previously loaded and selected file/module is not available
		// from the ProjectSourceParser at the startup, thus it is filled now
		if (DOCUMENT_TTCN3MODULE_MAP.get(document) == null) {
			for (IFile key : FILE_DOCUMENT_MAP.keySet()) {
				if (FILE_DOCUMENT_MAP.get(key).equals(document)) {
					final Module tempModule = GlobalParser.getProjectSourceParser(key.getProject()).containedModule(key);
					if (tempModule != null) {
						DOCUMENT_TTCN3MODULE_MAP.put(document, tempModule);
						return tempModule;
					}
				}
			}
		}
		return DOCUMENT_TTCN3MODULE_MAP.get(document);
	}
	
	public static AbstractDecoratedTextEditor get(final Module module) {
		if (module != null && module.getIdentifier() != null) {
			return MODULE_EDITOR.get(module.getIdentifier().getDisplayName());
		} else {
			return null;
		}
	}
}
