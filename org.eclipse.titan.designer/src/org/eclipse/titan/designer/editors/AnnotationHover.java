/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IMarker;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IInformationControl;
import org.eclipse.jface.text.IInformationControlCreator;
import org.eclipse.jface.text.source.IAnnotationHover;
import org.eclipse.jface.text.source.IAnnotationHoverExtension;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.text.source.ILineRange;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.LineRange;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.designer.editors.controls.MarkerHoverContent;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverInfoControl;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.texteditor.MarkerAnnotation;

/**
 * @author Kristof Szabados
 * @author Arpad Lovassy
 * @author Miklos Magyari
 */
public final class AnnotationHover implements IAnnotationHover, IAnnotationHoverExtension {

	@Override
	@Deprecated
	public String getHoverInfo(final ISourceViewer sourceViewer, final int lineNumber) {
		return null;
	}

	/**
	 * Helper function for {@link #getHoverInfo(ISourceViewer, int)}.
	 *
	 * @see #getHoverInfo(ISourceViewer, int)
	 *
	 * @param sourceViewer
	 *                The viewer whose annotation we wish to find.
	 * @param lineNumber
	 *                The line number in which the annotation(s) are.
	 * @return The list of markers residing in the given line in the given
	 *         editor.
	 */
	protected List<IMarker> getMarkerForLine(final ISourceViewer sourceViewer, final int lineNumber) {
		final List<IMarker> markers = new ArrayList<IMarker>();
		final IAnnotationModel annotationModel = sourceViewer.getAnnotationModel();
		if (annotationModel == null) {
			return markers;
		}

		final Iterator<?> iterator = annotationModel.getAnnotationIterator();
		while (iterator.hasNext()) {
			final Object o = iterator.next();
			if (o instanceof MarkerAnnotation) {
				final MarkerAnnotation actuaMarkerl = (MarkerAnnotation) o;
				try {
					final int actualLine = sourceViewer.getDocument().getLineOfOffset(
							annotationModel.getPosition(actuaMarkerl).getOffset());
					if (actualLine == lineNumber) {
						markers.add(actuaMarkerl.getMarker());
					}
				} catch (BadLocationException e) {
					ErrorReporter.logExceptionStackTrace(e);
				}
			}
		}
		return markers;
	}

	@Override
	public IInformationControlCreator getHoverControlCreator() {
		return new IInformationControlCreator() {
            @Override
            public IInformationControl createInformationControl(Shell parent) {
            	return new Ttcn3HoverInfoControl(parent, EditorsUI.getTooltipAffordanceString());
            }
        };
	}

	@Override
	public boolean canHandleMouseCursor() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Object getHoverInfo(ISourceViewer sourceViewer, ILineRange lineRange, int visibleNumberOfLines) {
		final List<IMarker> markers = getMarkerForLine(sourceViewer, lineRange.getStartLine());
		final StringBuilder builder = new StringBuilder();
		for (int i = 0; i < markers.size(); i++) {
			String message = markers.get(i).getAttribute(IMarker.MESSAGE, (String) null);
			if ( message != null ) {
				message = message.trim();
				if ( message.length() > 0 ) {
					builder.append( message );
				}
			}
		}
		return new MarkerHoverContent(builder.toString(), null);
	}

	@Override
	public ILineRange getHoverLineRange(ISourceViewer viewer, int lineNumber) {
		// FIXME calculate real range
		return new LineRange(lineNumber, 1);
	}
}
