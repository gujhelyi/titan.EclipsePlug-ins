/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.titan.common.logging.ErrorReporter;

/**
 * Collects some utility functions used related to editors
 * 
 * @author Adam Knapp
 *
 */
public final class EditorUtils {
	/** 
	 * Calculates the string of leading whitespaces for the given offset
	 * 
	 * @param document
	 * @param offset
	 * @param sb StringBuilder containing the leading whitesapces
	 * @return real start of the line (the first non-ws char)
	 */
	public static int getLeadingWhitespace(IDocument document, int offset, StringBuilder sb) {
		int realStart = offset;
		for (; offset >= 0; offset--) {
			try {
				final char c = document.getChar(offset);
				if (c == '\n') {
					break;
				}
				if (c == ' ' || c == '\t') {
					sb.append(c);
				} else {
					sb.setLength(0);
					realStart = offset;
				}
			} catch (BadLocationException e) {
				ErrorReporter.logExceptionStackTrace(e);
			}
		}
		return realStart;
	}
}
