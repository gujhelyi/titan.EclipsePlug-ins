/******************************************************************************
 * Copyright (c) 2000-2022 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.viewers.StyledString;
import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.Assignments;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definitions;
import org.eclipse.titan.designer.AST.TTCN3.definitions.ICommentable;
import org.eclipse.titan.designer.AST.TTCN3.definitions.TTCN3Module;
import org.eclipse.titan.designer.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.designer.AST.TTCN3.types.EnumItem;
import org.eclipse.titan.designer.AST.TTCN3.types.Referenced_Type;
import org.eclipse.titan.designer.AST.TTCN3.types.TTCN3_Enumerated_Type;
import org.eclipse.titan.designer.editors.CompletionProposal;
import org.eclipse.titan.designer.editors.ProposalCollector;
import org.eclipse.titan.designer.editors.Stylers;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverContent;
import org.eclipse.titan.designer.graphics.ImageCache;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.GlobalParser;
import org.eclipse.titan.designer.parsers.ProjectSourceParser;

/**
 * Base class for different proposal contexts
 * 
 * @author Miklos Magyari
 *
 */
public abstract class ProposalContext {
	public final static int HIGHEST = 16;
	public final static int TYPEANDPREFIX_MATCHES = 8;
	public final static int TYPE_MATCHES = 4;
	public final static int PREFIX_MATCHES = 2;
	public final static int OTHER = 1;
	
	protected ProjectSourceParser sourceParser;
	protected ProposalContextInfo proposalContextInfo;
	protected boolean doFallback;
	private CompilationTimeStamp timestamp;

	public ProposalContext(final ProposalContextInfo proposalContextInfo) {
		sourceParser = GlobalParser.getProjectSourceParser(proposalContextInfo.file.getProject());
		this.proposalContextInfo = proposalContextInfo;
		timestamp = proposalContextInfo.module.getLastCompilationTimeStamp();
		doFallback = true;
	}

	/**
	 * Adds context-specific proposals to the given collector
	 * 
	 * @param propCollector
	 */
	public abstract void getProposals(ProposalCollector propCollector);

	/**
	 * Collects the list of available definitions in the current scope
	 * 
	 * @param typeType
	 * @return
	 */
	protected List<Definition> getAvailableDefsByType(final Type_type typeType) {
		List<Definition> definitions = new ArrayList<Definition>();
		Map<String, Definition> map = null;

		final Scope sc = getScope();
		if (sc != null) {
			if (sc instanceof StatementBlock) {
				map = ((StatementBlock)sc).getDefinitionMap(); 
			} else if (sc instanceof Definitions) {
				map = ((Definitions)sc).getDefinitionMap();
			}
			
			if (map == null) {
				return definitions;
			}
			
			for (Map.Entry<String, Definition> e : map.entrySet()) {
				final Definition def = e.getValue();
				final IType deftype = def.getType(timestamp);
				if (deftype != null && deftype.getTypetype() == typeType) {
					definitions.add(def);
				}
			}
		}

		final List<Module> importedModules = proposalContextInfo.module.getImportedModules();
		for (final Module importEntry : importedModules) {
			final Scope moduleScope = importEntry.getModuleScope();
			if (moduleScope instanceof TTCN3Module) {
				final TTCN3Module module = (TTCN3Module)moduleScope;
				final Definitions importDefinitions = module.getDefinitions();
				for (int i = 0; i < importDefinitions.getNofAssignments(); i++) {
					final Definition definitionEntry = importDefinitions.getAssignmentByIndex(i);
					final IType entryType = definitionEntry.getType(timestamp);
					if (entryType instanceof Referenced_Type) {
						if (((Referenced_Type)entryType).getTypeRefdLast(timestamp).getTypetypeTtcn3() == typeType) {
							definitions.add(definitionEntry);
						}
					} else if (entryType != null && entryType.getTypetype() == typeType) {
						definitions.add(definitionEntry);
					}
				}
			}
		}
		return definitions;
	}

	protected List<Assignment> getAvailableAssignmentsByType(final Type_type typeType) {
		List<Assignment> assignments = new ArrayList<Assignment>();

		final Scope sc = getScope();
		if (sc == null) {
			return assignments; 
		}

		final Assignments asses = proposalContextInfo.scope.getAssignmentsScope();
		final int nrAss = asses.getNofAssignments();
		for (int i = 0; i < nrAss; i++) {
			Assignment ass = asses.getAssignmentByIndex(i);
			IType aType = ass.getType(timestamp);
			if (aType != null) {
				if (ass.getType(timestamp).getTypetype() == typeType) {
					assignments.add(ass);
				}
			}
		}
	
		return assignments;
	}

	private Scope getScope() {
		// FIXME : timestamp should not be null; this check is to avoid stack overflow
		if (timestamp == null) {
			return null;
		}

		Scope sc = proposalContextInfo.module.getSmallestEnclosingScope(proposalContextInfo.offset - getPrefix().length());
		while (sc != null) {
			if (sc instanceof Definitions || sc instanceof StatementBlock) {
				break;
			}
			sc = sc.getParentScope();
		}
		return sc;		
	}

	/**
	 * Returns whether fallback is needed to the old proposal list compilation
	 * @return Whether fallback is needed to the old proposal list compilation
	 */
	public boolean doFallback() {
		return doFallback;
	}

	/**
	 * Returns the prefix based on the context information. Never returns {@code null}.
	 * @return the prefix based on the context information
	 */
	protected String getPrefix() {
		if (proposalContextInfo.context != null && !proposalContextInfo.context.isEmpty()) {
			final StringBuilder sb = new StringBuilder(proposalContextInfo.context.length());
			char c;
			for (int i = proposalContextInfo.context.length()-1; i >= 0; i--) {
				c = proposalContextInfo.context.charAt(i);
				if (!isPrefixChar(c)) {
					break;
				}
				sb.insert(0, c);
			}
			return sb.toString();
		} else {
			return "";
		}
	}

	/**
	 * Returns whether the specified char is considered as part of a prefix.
	 * Sub-classes may override this method.
	 * @param c
	 * @return whether the specified char is considered as part of a prefix
	 */
	protected boolean isPrefixChar(final char c) {
		return Character.isAlphabetic(c) || Character.isDigit(c) || c == '_';
	}
	
	/**
	 * Adds available items of the specified type to the proposal list
	 *  
	 * @param type
	 * @param ttype
	 * @param propCollector
	 * @param excludeName
	 */
	protected void addItemsByType(IType type, Type_type ttype, ProposalCollector propCollector, String prefixIn, String excludeName) {
		final String prefix = prefixIn != null ? prefixIn : "";
		final int replacementOffset = proposalContextInfo.offset - prefix.length();
		final int replacementLength = prefix.length();
		
		switch (ttype) {
		case TYPE_TTCN3_ENUMERATED:
			TTCN3_Enumerated_Type etype = (TTCN3_Enumerated_Type)type;
			for (EnumItem item : etype.getEnumItems()) {
				final String enumString = item.getId().getDisplayName();
				final Ttcn3HoverContent enumItemHover = etype.getItemHoverContent(item.getId());
				final CompletionProposal enumProposal = new CompletionProposal(enumString, replacementOffset, replacementLength, 
						enumString.length(), ImageCache.getImageByType(ttype), new StyledString(enumString, new Stylers.ItalicStyler()), 
						null, enumItemHover, enumString.toLowerCase().startsWith(prefix.toLowerCase()) ? HIGHEST | PREFIX_MATCHES : TYPE_MATCHES);
				propCollector.addProposal(enumProposal);
			}
			List<Definition> enumDefList = getAvailableDefsByType(ttype);
			for (Definition def : enumDefList) {
				if (def.getType(timestamp) instanceof Referenced_Type) {
					final Referenced_Type enumref = (Referenced_Type)def.getType(timestamp);
					if (enumref.getTypeRefdLast(timestamp) instanceof TTCN3_Enumerated_Type) {
						final String ename = def.getIdentifier().getDisplayName();
						Ttcn3HoverContent enumHover = null;
						if (def instanceof ICommentable) {
							enumHover = ((ICommentable)def).getHoverContent(null);
						}
						final CompletionProposal enumProposal2 = new CompletionProposal(ename, replacementOffset, replacementLength, 
							ename.length(), ImageCache.getImageByType(ttype), new StyledString(ename), 
							null, enumHover , ename.toLowerCase().startsWith(prefix.toLowerCase()) ? TYPE_MATCHES | PREFIX_MATCHES : TYPE_MATCHES);
							propCollector.addProposal(enumProposal2);
					}
				}
			}
			break;
		case TYPE_BOOL:
			final String trueString = "true";
			final CompletionProposal trueProposal = new CompletionProposal(trueString, replacementOffset, replacementLength, 
					trueString.length(), ImageCache.getImageByType(ttype), new StyledString(trueString, new Stylers.ItalicStyler()), 
					null, null, trueString.toLowerCase().startsWith(prefix.toLowerCase()) ? HIGHEST | PREFIX_MATCHES : TYPE_MATCHES);
			final String falseString = "false";
			final CompletionProposal falseProposal = new CompletionProposal(falseString, replacementOffset, replacementLength, 
					falseString.length(), ImageCache.getImageByType(ttype), new StyledString(falseString, new Stylers.ItalicStyler()), 
					null, null, falseString.toLowerCase().startsWith(prefix.toLowerCase()) ? HIGHEST | PREFIX_MATCHES : TYPE_MATCHES);
			propCollector.addProposal(trueProposal);
			propCollector.addProposal(falseProposal);
			/** no break on purpose */
		case TYPE_BITSTRING:
		case TYPE_CHARSTRING:
		case TYPE_HEXSTRING:
		case TYPE_INTEGER:
		case TYPE_OCTETSTRING:
		case TYPE_REAL:
		case TYPE_UCHARSTRING:
			List<Definition> defList = getAvailableDefsByType(ttype);
			for (Definition a : defList) {
				final String name = a.getIdentifier().getDisplayName();
				if (! name.equals(excludeName)) {
					final Ttcn3HoverContent doc = a.getHoverContent(null);
					final CompletionProposal proposal = new CompletionProposal(name, replacementOffset, replacementLength, 
						name.length(), ImageCache.getImageByType(ttype), new StyledString(name), 
						null, doc, name.toLowerCase().startsWith(prefix.toLowerCase()) ? TYPE_MATCHES | PREFIX_MATCHES : TYPE_MATCHES);
					propCollector.addProposal(proposal);
				}
			}
			List<Assignment> aList = getAvailableAssignmentsByType(ttype);
			for (Assignment a : aList) {
				switch (a.getAssignmentType()) {
				case A_TYPE:
					break;
				default:
					Ttcn3HoverContent doc = null;
					if (a instanceof ICommentable) {						
						final ICommentable d = (ICommentable)a;
						doc = d.getHoverContent(null);
					}
					final String name = a.getIdentifier().getDisplayName();
					final CompletionProposal proposal = new CompletionProposal(name, replacementOffset, replacementLength, 
						name.length(), ImageCache.getImageByType(ttype), new StyledString(name), 
						null, doc, name.toLowerCase().startsWith(prefix.toLowerCase()) ? TYPE_MATCHES | PREFIX_MATCHES : TYPE_MATCHES);
					propCollector.addProposal(proposal);
				}
			}
			break;
		default:
		}
		propCollector.sortAll();
	}
}
