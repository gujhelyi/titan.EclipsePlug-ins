/******************************************************************************
 * Copyright (c) 2000-2022 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import org.eclipse.jface.viewers.StyledString;
import org.eclipse.titan.designer.AST.TTCN3.definitions.ICommentable;
import org.eclipse.titan.designer.editors.CompletionProposal;
import org.eclipse.titan.designer.editors.ProposalCollector;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverContent;
import org.eclipse.titan.designer.graphics.ImageCache;

/**
 * Collects proposals for documentation comment context
 * 
 * @author Adam Knapp 
 *
 */
public class DocumentCommentContext extends ProposalContext {

	private static final String[] HTML_TAGS = { "<b>", "<code>", "<em>", "<li>", "<ol>", "<p>", "<pre>",
			"<strong>", "<ul>" };
	private static final String LINE_BREAK = "<br>";

	public DocumentCommentContext(final ProposalContextInfo proposalContextInfo) {
		super(proposalContextInfo);
		doFallback = false;
	}

	@Override
	public void getProposals(ProposalCollector propCollector) {
		String prefix = getPrefix();
		if (!prefix.isEmpty()) {
			if (prefix.startsWith("<")) {
				addHtmlTagProposals(propCollector, prefix);
				return;
			} else if (!prefix.startsWith(ICommentable.AT)) {
				prefix = ICommentable.AT + prefix;
			}
		}
		addDocCommentTagProposals(propCollector, prefix);
	}

	@Override
	protected boolean isPrefixChar(final char c) {
		return Character.isAlphabetic(c) || Character.isDigit(c) || c == '_' || c == '@' || c == '<';
	}

	private void addDocCommentTagProposals(ProposalCollector propCollector, String prefix) {
		final int replacementOffset = proposalContextInfo.offset - prefix.length();
		final int replacementLength = prefix.length();
		for (String tag : ICommentable.DOC_COMMENT_TAGS) {
			final CompletionProposal proposal = new CompletionProposal(tag, replacementOffset, replacementLength, 
				tag.length(), ImageCache.getImage(proposalContextInfo.module.getOutlineIcon()), 
				new StyledString(tag), null, new Ttcn3HoverContent(tag + " tag"), tag.startsWith(prefix) ? PREFIX_MATCHES : OTHER);

			propCollector.addProposal(proposal);
		}

		propCollector.sortAll();
	}

	private void addHtmlTagProposals(ProposalCollector propCollector, String prefix) {
		final int replacementOffset = proposalContextInfo.offset - prefix.length();
		final int replacementLength = prefix.length();
		for (String tag : HTML_TAGS) {
			final CompletionProposal proposal = new CompletionProposal(tag + getHtmlClosingTag(tag),
				replacementOffset, replacementLength, tag.length(), ImageCache.getImage(proposalContextInfo.module.getOutlineIcon()), 
				new StyledString(tag), null, new Ttcn3HoverContent(tag + " html tag"), tag.startsWith(prefix) ? PREFIX_MATCHES : OTHER);

			propCollector.addProposal(proposal);
		}
		final CompletionProposal proposal = new CompletionProposal(LINE_BREAK, replacementOffset, replacementLength, 
				LINE_BREAK.length(), ImageCache.getImage(proposalContextInfo.module.getOutlineIcon()), 
				new StyledString(LINE_BREAK), null, new Ttcn3HoverContent(LINE_BREAK + "html tag"), LINE_BREAK.startsWith(prefix) ? PREFIX_MATCHES : OTHER);
		propCollector.addProposal(proposal);

		propCollector.sortAll();
	}

	private String getHtmlClosingTag(final String tag) {
		final StringBuilder sb = new StringBuilder(tag);
		return sb.insert(1, '/').toString();
	}
}
