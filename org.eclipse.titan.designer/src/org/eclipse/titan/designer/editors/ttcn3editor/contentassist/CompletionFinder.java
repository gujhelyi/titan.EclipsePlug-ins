/******************************************************************************
 * Copyright (c) 2000-2022 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.titan.common.logging.ErrorReporter;
import org.eclipse.titan.designer.AST.Module;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.ui.IEditorPart;

/**
 * This class tries to find out the context for the current cursor position
 * It can be used to provide more suitable content assist proposals
 * 
 * @author Miklos Magyari
 * @author Adam Knapp
 *
 */
public class CompletionFinder {	
	private static final String IDENTIFIER = "([A-Za-z][A-Za-z0-9_]*)";
	private static final String ASSIGNOP = ":="; 
	private static final String DEFINITION = "\\s+" + IDENTIFIER + "\\s+" + IDENTIFIER + "\\s*" + ASSIGNOP;
	private static final String ASSIGNMENT = "(var|const)" + DEFINITION + "\\s*(" + IDENTIFIER + ")?$";
	private static final String FRIEND = "friend[\\s]+module[\\s]*(.*)";
	private static final String IMPORT = "import[\\s]+from([\\s]*$|[\\s+]+(" + IDENTIFIER + "))";
	private static final String COMPONENT = "(system|mtc|runs[\\s]+on)((\\s+" + IDENTIFIER + "$)|\\s*)";
	private static final String PARAMETERIZED = IDENTIFIER + "\\s*\\(([^)]*$)";
	
	private enum SpecialContext { None, SimpleComment, DocComment, StringLiteral }
	private enum ContextType { Assignment, Component, Doccomment, Friend, Import, Parameterized }
	
	private IEditorPart editor;
	private Module module;
	private Scope scope;
	private IDocument document;
	private IFile file;
	private int offset;
	
	/** collection of context tokens */
	private List<String> tokenList = new ArrayList<>();
	
	/** collection of whitespace strings separating context tokens */
	private List<String> wsList = new ArrayList<>();
	
	private static Map<Pattern,ContextType> contextMap = new LinkedHashMap<Pattern, ContextType>();

	static {		
		contextMap.put(Pattern.compile(COMPONENT), ContextType.Component);
		contextMap.put(Pattern.compile(ASSIGNMENT), ContextType.Assignment);
		contextMap.put(Pattern.compile(IMPORT), ContextType.Import);
		contextMap.put(Pattern.compile(FRIEND), ContextType.Friend);
		contextMap.put(Pattern.compile(PARAMETERIZED), ContextType.Parameterized);
	}

	public CompletionFinder(Module module) {
		this.module = module;
	}

	public ProposalContext findOnLeft(final IEditorPart editor, int offset, final IDocument document, final IFile file) {
		this.editor = editor;
		this.document = document;
		this.file = file;
		this.offset = offset;
		int ofs = offset - 1;
		if (-1 == ofs) {
			return null;
		}

		final StringBuilder left = new StringBuilder();

		StringBuilder specialStringSb = new StringBuilder();
		final SpecialContext commentContext = getSpecialContext(specialStringSb);
		switch (commentContext) {
		case SimpleComment:
		case StringLiteral:
			return null;
		case DocComment:
			return new DocumentCommentContext(new ProposalContextInfo(file, document, offset, specialStringSb.toString(), module, null));
		default:
			break;
		}

		try {
			char c;

			String ws = getWsStringAtOffset(ofs);
			if (ws.length() > 0) {
				wsList.add(ws);
				ofs -= ws.length();
				left.insert(0, ' ');
			}

			final StringBuilder token = new StringBuilder();
			while (ofs >= 0) {
				c = document.getChar(ofs);
				if (Character.isAlphabetic(c) || Character.isDigit(c) || c == ',' || c == ':' || c == '=' || c == '(') {
					token.insert(0, c);
					ofs--; 
					if (-1 == ofs) {
						return getContext(left.toString());
					}
				} else if (isWhiteSpace(c)) {
					ws = getWsStringAtOffset(ofs);
					if (ws.length() > 0) {
						wsList.add(ws);
						ofs -= ws.length();
						
						left.insert(0, token.toString());
						left.insert(0, " ");
						token.setLength(0);
					}
				} else {
					break;
				}
			}
		} catch (BadLocationException e) {
			ErrorReporter.logExceptionStackTrace(e);
		}

		return getContext(left.toString());
	}

	private String getWsStringAtOffset(int offset) {
		final StringBuilder wslist = new StringBuilder();

		for (;;) {
			char c;
			try {
				c = document.getChar(offset);
			} catch (BadLocationException e) {
				return wslist.toString();
			}
			if (isWhiteSpace(c)) {
				wslist.append(c);
				offset--;
				if (offset == -1) {
					return wslist.toString();
				}
			} else {
				break;
			}
		}
		return wslist.toString();
	}

	private ProposalContext getContext(String s) {
		if (s == null || s.isEmpty()) {
			return null;
		}		

		for (Map.Entry<Pattern,ContextType> e : contextMap.entrySet()) {
			Pattern p = e.getKey();
			Matcher m = p.matcher(s);
			if (m.find()) {
				final ProposalContextInfo info = new ProposalContextInfo(file, document, offset, s, module, m);
				switch (e.getValue()) {
				case Assignment:
					return new AssignmentContext(info);
				case Component:
					return new ComponentContext(info);
				case Friend:
					return new FriendContext(info);
				case Import:
					return new ImportContext(info);
				case Parameterized:
					return new ParameterizedContext(info);
				default:
					return null;
				}
			}
		}

		return null;
	}

	private static boolean isWhiteSpace(char c) {
		return c == ' ' || c == '\t' || c == '\n' || c == '\r';
	}

	/**
	 * Calculates if the current offset is inside a comment block or a string literal
	 * 
	 * @param 
	 * @return
	 */
	private SpecialContext getSpecialContext(final StringBuilder specialStringSb) {
		boolean inComment = false;
		boolean inString = false;
		boolean isLineComment = false;
		boolean isDocComment = false;
		int lastStartOffset = -1;
		String docText = null;
		try {
			docText = document.get(0, offset + 1);
		} catch (BadLocationException e1) {
			return SpecialContext.None;
		}
		for (int i = 0; i < offset; i++) {
			char c, c2, c3;
			c = docText.charAt(i);
			boolean isEof = false;
			if (inString) {
				do {
					c2 = docText.charAt(i++);
					if (c2 == '\\') {
						i++;
					}
					if (i >= offset) {
						isEof = true;
					}
				} while (c2 != '"' && i < offset);
				if (isEof == false) {
					inString = false;
					i--;
				}
			} else if (inComment) {
				if (isLineComment) {
					do {
						c2 = docText.charAt(i++);
						if (i >= offset) {
							isEof = true;
						}
					} while (c2 != '\n' && i < offset);
				} else {
					do {
						c2 = docText.charAt(i++);
						if (i >= offset) {
							isEof = true;
						}
						c3 = docText.charAt(i);
					} while (i < offset && ! (c2 == '*' && c3 == '/'));
				}
				if (isEof == false) {
					inComment = false;
					i--;
				}
			} else {
				if (c == '/') {
					lastStartOffset = i;
					c2 = docText.charAt(i + 1);
					if (c2 == '*') {
						inComment = true;
						isLineComment = false;
						i++;
					} else if (c2 == '/') {
						inComment = true;
						isLineComment = true;
						i++;
					}
					if (inComment) {
						c2 = docText.charAt(i + 1);
						if (c2 == '*') {
							isDocComment = true;
							i++;
						} else {
							isDocComment = false;
						}
					}
				} else if (c == '"') {
					lastStartOffset = i;
					inString = true;
				}
			}
		}
		if (lastStartOffset != -1) {
			specialStringSb.append(docText.substring(lastStartOffset, offset));
		}
		if (inComment) {
			if (isDocComment) {
				return SpecialContext.DocComment;
			}
			return SpecialContext.SimpleComment;
		}
		if (inString) {
			return SpecialContext.StringLiteral;
		}
		
		return SpecialContext.None;
	}

	public List<String> getTokenList() {
		return tokenList;
	}

	public List<String> getWsList() {
		return wsList;
	}
}
