/******************************************************************************
 * Copyright (c) 2000-2022 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors.ttcn3editor.contentassist;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;

import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.IType.Type_type;
import org.eclipse.titan.designer.editors.ProposalCollector;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReferenceAnalyzer;

/**
 * Collects proposals for an assignment statement
 * 
 * @author Miklos Magyari
 *
 */
public class AssignmentContext extends ProposalContext {
	private static Map<String,Type_type> basicTypes = new HashMap<String,Type_type>();
	private AssignmentMatch aMatch;
	
	static {
		basicTypes.put("boolean", Type_type.TYPE_BOOL);
		basicTypes.put("bitstring", Type_type.TYPE_BITSTRING);
		basicTypes.put("charstring", Type_type.TYPE_CHARSTRING);
		basicTypes.put("float", Type_type.TYPE_REAL);
		basicTypes.put("hexstring", Type_type.TYPE_HEXSTRING);
		basicTypes.put("integer", Type_type.TYPE_INTEGER);
		basicTypes.put("octetstring", Type_type.TYPE_OCTETSTRING);
	}
	
	public AssignmentContext(final ProposalContextInfo proposalContextInfo) {
		super(proposalContextInfo);
		
		aMatch = getAssignmentMatch();
		doFallback = false;
	}

	@Override
	public void getProposals(ProposalCollector propCollector) {
		final CompilationTimeStamp timestamp = proposalContextInfo.module.getLastCompilationTimeStamp();
	
		if (aMatch.reference == null && aMatch.typeType == null) {
			return;
		}
		
		if (proposalContextInfo.scope == null) {
			return;
		}
		IType type = null;
		Type_type ttype;
		if (aMatch.typeType != null) {
			ttype = aMatch.typeType;
		} else {
			Assignment assignment = proposalContextInfo.scope.getAssBySRef(timestamp, aMatch.reference);
			if (assignment == null) {
				return;
			}
			type = assignment.getType(timestamp); 
			if (type == null) {
				return;
			}
			ttype = type.getTypetypeTtcn3();
		}
		addItemsByType(type, ttype, propCollector, aMatch.prefix != null ? aMatch.prefix : "", aMatch.name);
	}
	
	private AssignmentMatch getAssignmentMatch() {
		AssignmentMatch assignmentMatch = new AssignmentMatch();
		final Matcher m = proposalContextInfo.matcher;
		final String vartype = m.group(2);
		final Type_type typeType = basicTypes.get(vartype);
		if (typeType != null) {
			assignmentMatch.typeType = typeType;
		} else {				
			assignmentMatch.reference = TTCN3ReferenceAnalyzer.parseForCompletion(proposalContextInfo.file, vartype);
		}
		assignmentMatch.name = m.group(3);
		assignmentMatch.prefix = m.group(4);
		
		return assignmentMatch;
	}
}
