/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.JFacePreferences;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.StringConverter;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;
import org.eclipse.titan.common.utils.BundleUtils;
import org.eclipse.titan.designer.Activator;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.ui.PlatformUI;
import org.osgi.framework.Version;
import org.osgi.service.prefs.Preferences;

/**
 * This class manages the Color resources, so that we can reach them by provided
 * attributes.
 *
 * @author Kristof Szabados
 * @author Adam Knapp
 * @author Miklos Magyari
 */
public final class ColorManager {
	public enum Theme { Light, Dark }

	private static final Map<String, Color> COLOR_TABLE = new HashMap<String, Color>(10);
	private static final Map<String, Token> TOKEN_TABLE = new HashMap<String, Token>();

	/** Workaround for JFace backward incompatibility */
	private static final String INFORMATION_BACKGROUND_COLOR = "org.eclipse.ui.workbench.INFORMATION_BACKGROUND";

	/**
	 * Gets foreground color associated with a color preference name.
	 *
	 * @param aName The name of the preference.
	 * @param store Preference store from which the preference is loaded.
	 * @return The background color associated with a color preference name,
	 *         or if one can not be found, the default foreground color of
	 *         the system.
	 */
	public Color getForegroundColor(final String aName, final IPreferenceStore store) {
		Color color = COLOR_TABLE.get(aName);
		if (color == null) {
			final RGB rgb = getColor(store, aName);
			if (rgb == null) {
				color = getDisplay().getSystemColor(SWT.COLOR_LIST_FOREGROUND);
			} else {
				color = new Color(getDisplay(), rgb);
			}
			COLOR_TABLE.put(aName, color);
		}
		return color;
	}

	/**
	 * Gets background color associated with a color preference name.
	 *
	 * @param aName The name of the preference.
	 * @param store Preference store from which the preference is loaded.
	 * @return The background color associated with a color preference name,
	 *         or if one can not be found, then white.
	 */
	public Color getBackgroundColor(final String aName, final IPreferenceStore store) {
		Color color = COLOR_TABLE.get(aName);
		if (color == null) {
			final RGB rgb = getColor(store, aName);
			if (rgb == null) {
				color = getDisplay().getSystemColor(SWT.COLOR_WHITE);
			} else {
				color = new Color(getDisplay(), rgb);
			}
			COLOR_TABLE.put(aName, color);
		}
		return color;
	}

	/**
	 * Creates a TextAttribute out of a 'preference group' 's name.
	 * <p>
	 * Actually 3 preference names are use which are postfixed to get the actual ones needed.
	 *
	 * @see #getBackgroundColor(String)
	 * @see #getForegroundColor(String)
	 * @see #createTokenFromPreference(String)
	 * @see PreferenceConstants
	 *
	 * @param key The 'preference group' 's name.
	 * @return The TextAttribute created.
	 */
	public TextAttribute createAttributeFromPreference(final String key) {
		final IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		final Color foregroundColor = getForegroundColor(key + PreferenceConstants.FOREGROUND, store);
		Color backgroundColor;
		if (store.getBoolean(key + PreferenceConstants.USEBACKGROUNDCOLOR)) {
			backgroundColor = getBackgroundColor(key + PreferenceConstants.BACKGROUND, store);
		} else {
			backgroundColor = null;
		}

		int style = SWT.None;
		if (store.getBoolean(key + PreferenceConstants.BOLD)) {
			style |= SWT.BOLD;
		}
		if (store.getBoolean(key + PreferenceConstants.ITALIC)) {
			style |= SWT.ITALIC;
		}
		if (store.getBoolean(key + PreferenceConstants.STRIKETHROUGH)) {
			style |= TextAttribute.STRIKETHROUGH;
		}
		if (store.getBoolean(key + PreferenceConstants.UNDERLINE)) {
			style |= TextAttribute.UNDERLINE;
		}
		return new TextAttribute(foregroundColor, backgroundColor, style != SWT.None ? style : SWT.NORMAL);
	}

	/**
	 * Creates a Token out of a 'preference group' 's name.
	 *
	 * @see #createAttributeFromPreference(String)
	 *
	 * @param key
	 *                The 'preference group' 's name.
	 * @return The Token created.
	 */
	public Token createTokenFromPreference(final String key) {
		if (TOKEN_TABLE.containsKey(key)) {
			return TOKEN_TABLE.get(key);
		}

		final Token temp = new Token(createAttributeFromPreference(key));
		TOKEN_TABLE.put(key, temp);
		return temp;
	}

	/**
	 * Creates a {@linkplain StyleRange} based on the specified key loaded from the specified preference store.
	 * @param key The name of the 'preference group'
	 * @param prefStore Preference store from which the preference is loaded
	 * @param start start offset of the style
	 * @param length  length of the style
	 * @return The {@linkplain StyleRange} created
	 */
	public StyleRange createStyleRangeFromPreference(final String key, final IPreferenceStore prefStore,
			final int start, final int length) {
		IPreferenceStore store;
		if (prefStore != null) {
			store = Activator.getDefault().getPreferenceStore();
		} else {
			store = Activator.getDefault().getPreferenceStore();
		}

		final Color foregroundColor = getForegroundColor(key + PreferenceConstants.FOREGROUND, store);
		Color backgroundColor;
		if (store.getBoolean(key + PreferenceConstants.USEBACKGROUNDCOLOR)) {
			backgroundColor = getBackgroundColor(key + PreferenceConstants.BACKGROUND, store);
		} else {
			backgroundColor = null;
		}

		int style = SWT.None;
		if (store.getBoolean(key + PreferenceConstants.BOLD)) {
			style |= SWT.BOLD;
		}
		if (store.getBoolean(key + PreferenceConstants.ITALIC)) {
			style |= SWT.ITALIC;
		}
		final StyleRange sr = new StyleRange(start, length,	foregroundColor, backgroundColor, style);
		sr.strikeout = store.getBoolean(key + PreferenceConstants.STRIKETHROUGH);
		sr.underline = store.getBoolean(key + PreferenceConstants.UNDERLINE);
		return sr;
	}

	/**
	 * Updates the Token that handles the data related to the provided key.
	 * It does this by, removing the previously defined attributes from the
	 * related Token, and recalculates them.
	 *
	 * @param key
	 *                the PreferenceConstant element, who's attributes are
	 *                to be re-evaluated
	 * */
	public void update(final String key) {
		String baseKey = null;
		if (key.endsWith(PreferenceConstants.FOREGROUND)) {
			baseKey = key.substring(0, key.length() - PreferenceConstants.FOREGROUND.length());
		} else if (key.endsWith(PreferenceConstants.BACKGROUND)) {
			baseKey = key.substring(0, key.length() - PreferenceConstants.BACKGROUND.length());
		} else if (key.endsWith(PreferenceConstants.USEBACKGROUNDCOLOR)) {
			baseKey = key.substring(0, key.length() - PreferenceConstants.USEBACKGROUNDCOLOR.length());
		} else if (key.endsWith(PreferenceConstants.BOLD)) {
			baseKey = key.substring(0, key.length() - PreferenceConstants.BOLD.length());
		} else if (key.endsWith(PreferenceConstants.ITALIC)) {
			baseKey = key.substring(0, key.length() - PreferenceConstants.ITALIC.length());
		} else if (key.endsWith(PreferenceConstants.STRIKETHROUGH)) {
			baseKey = key.substring(0, key.length() - PreferenceConstants.STRIKETHROUGH.length());
		} else if (key.endsWith(PreferenceConstants.UNDERLINE)) {
			baseKey = key.substring(0, key.length() - PreferenceConstants.UNDERLINE.length());
		}

		if (baseKey != null && TOKEN_TABLE.containsKey(baseKey)) {
			final Token tempToken = TOKEN_TABLE.get(baseKey);
			COLOR_TABLE.remove(key);
			tempToken.setData(createAttributeFromPreference(baseKey));
		}
	}

	/**
	 * Returns the theme specific information background color or white in case of older JFace versions
	 * @return the theme specific information background color or white in case of older JFace versions
	 * @see JFacePreferences#INFORMATION_BACKGROUND_COLOR
	 */
	public static Color getInformationBackgroundColor() {
		final Version since = new Version(3, 14, 0);
		if (BundleUtils.getJFaceVersion().compareTo(since) >= 0) {
			return JFaceResources.getColorRegistry().get(INFORMATION_BACKGROUND_COLOR);
		}
		return Display.getCurrent().getSystemColor(SWT.COLOR_WHITE);
	}

	/**
	 * Gets the currently set color theme
	 * @return
	 */
	public static Theme getColorTheme() {
		final Preferences preferences = InstanceScope.INSTANCE.getNode("org.eclipse.e4.ui.css.swt.theme");
		final String colorTheme = preferences.get("themeid", "default");

		if (colorTheme.toLowerCase().contains("dark")) {
			return Theme.Dark;
		} else {
			return Theme.Light;
		}
	}

	/**
	 * Returns the current value of the color-valued preference with the
	 * given name in the given preference store.
	 * Returns the default-default value ({@code null}
	 * if there is no preference with the given name, or if the current value
	 * cannot be treated as a color.
	 * Based on {@link org.eclipse.jface.preference.PreferenceConverter#getColor}
	 *
	 * @param store the preference store
	 * @param name the name of the preference
	 * @return the color-valued preference
	 * 
	 * @see org.eclipse.jface.preference.PreferenceConverter#getColor
	 */
	private static RGB getColor(IPreferenceStore store, String name) {
		return StringConverter.asRGB(store.getString(name), null);
	}

	private static Display getDisplay() {
		return PlatformUI.getWorkbench().getDisplay();
	}
}
