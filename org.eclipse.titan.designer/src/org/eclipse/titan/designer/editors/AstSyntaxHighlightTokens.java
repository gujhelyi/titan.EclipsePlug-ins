/******************************************************************************
 * Copyright (c) 2000-2022 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.editors;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.collections.api.map.sorted.MutableSortedMap;
import org.eclipse.collections.api.tuple.Pair;
import org.eclipse.collections.impl.map.mutable.primitive.IntObjectHashMap;
import org.eclipse.collections.impl.map.sorted.mutable.TreeSortedMap;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.IPreferencesService;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.titan.designer.Activator;
import org.eclipse.titan.designer.AST.Module.SyntaxDecoration;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.titan.designer.productUtilities.ProductConstants;

/**
 * This class handles the map of token offsets used for AST based semantic decoration
 * and also keeps track of bracket depths at certain offsets 
 * 
 * @author Miklos Magyari
 *
 */
public final class AstSyntaxHighlightTokens {
	private static Map<String, IntObjectHashMap<IToken>> offsetMaps = new HashMap<>();
	private static Map<String, MutableSortedMap<Integer,Integer>> depthMaps = new HashMap<>();
	
	private static final int MAX_BRACKET_DEPTH = 4;
	
	private static SyntaxDecoration[] brackets = {
		SyntaxDecoration.Bracket0,
		SyntaxDecoration.Bracket1,
		SyntaxDecoration.Bracket2,
		SyntaxDecoration.Bracket3,
		SyntaxDecoration.Bracket4,
		SyntaxDecoration.Bracket5,
		SyntaxDecoration.Bracket6,
		SyntaxDecoration.Bracket7,
		SyntaxDecoration.Bracket8,
		SyntaxDecoration.Bracket9
	};
	
	private static Map<SyntaxDecoration, IToken> tokenMap = new HashMap<>();
	
	static {
		final IPreferencesService prefs = Platform.getPreferencesService();
		final boolean isSemanticHighlightingEnabled = prefs.getBoolean(ProductConstants.PRODUCT_ID_DESIGNER,
				PreferenceConstants.ENABLE_SEMANTIC_HIGHLIGHTING, false, null);
		if (isSemanticHighlightingEnabled) {
			addTokens();
		}
		
		Activator.getDefault().getPreferenceStore().addPropertyChangeListener((event) -> {
			final String property = event.getProperty();
			if (PreferenceConstants.ENABLE_SEMANTIC_HIGHLIGHTING.equals(property)) {
				final boolean isEnabled = prefs.getBoolean(ProductConstants.PRODUCT_ID_DESIGNER,
						PreferenceConstants.ENABLE_SEMANTIC_HIGHLIGHTING, false, null);
				if (isEnabled) {
					addTokens();
				}
			}
		});
	}
	
	private static void addTokens() {
		final ColorManager colorManager = new ColorManager();
		tokenMap.put(SyntaxDecoration.Bracket0, colorManager.createTokenFromPreference(PreferenceConstants.BRACKETCOLOR0));
		tokenMap.put(SyntaxDecoration.Bracket1, colorManager.createTokenFromPreference(PreferenceConstants.BRACKETCOLOR1));
		tokenMap.put(SyntaxDecoration.Bracket2, colorManager.createTokenFromPreference(PreferenceConstants.BRACKETCOLOR2));
		tokenMap.put(SyntaxDecoration.Bracket3, colorManager.createTokenFromPreference(PreferenceConstants.BRACKETCOLOR3));
		tokenMap.put(SyntaxDecoration.Bracket4, colorManager.createTokenFromPreference(PreferenceConstants.BRACKETCOLOR4));
	}
	
	/** no instantiation */
	private AstSyntaxHighlightTokens() {
		
	}
	
	public static synchronized IntObjectHashMap<IToken> getOffsetMap(String moduleName) {
		final IntObjectHashMap<IToken> existingMap = offsetMaps.get(moduleName);
		if (existingMap != null) {
			return existingMap;
		} else {
			IntObjectHashMap<IToken> newOffsetMap = new IntObjectHashMap<IToken>();
			offsetMaps.put(moduleName, newOffsetMap);
			return newOffsetMap;
		}
	}
	
	public static synchronized MutableSortedMap<Integer,Integer> getDepthMap(String moduleName) {
		final MutableSortedMap<Integer,Integer> existingMap = depthMaps.get(moduleName);
		if (existingMap != null) {
			return existingMap;
		} else {
			final MutableSortedMap<Integer,Integer> newDepthMap = new TreeSortedMap<Integer,Integer>();
			depthMaps.put(moduleName, newDepthMap);
			return newDepthMap;
		}
	}
	
	public static synchronized void setOffsetMap(String module, IntObjectHashMap<IToken> offsetMap) {
		offsetMaps.put(module, offsetMap);
	}
	
	public static synchronized void addBracket(String module, IFile actualFile, int offset, int depth) {
		final IntObjectHashMap<IToken> tokens = getOffsetMap(module);
		final MutableSortedMap<Integer,Integer> depths = getDepthMap(actualFile.getFullPath().toOSString());
		
		if (depth > MAX_BRACKET_DEPTH || depth < 0) {
			tokens.remove(offset);
			depths.remove(offset);
			return;
		}
		
		tokens.put(offset, tokenMap.get(brackets[depth]));		
		depths.put(offset, depth);
	}
	
	/**
	 * Gets bracket depth at the given offset of a file. It is needed for incremental reparsing to know
	 * how deep the reparsed code is located in the bracket structure 
	 * 
	 * @param file
	 * @param offset
	 * @return
	 */
	public static int getDepthAtOffset(IFile file, int offset) {
		final MutableSortedMap<Integer,Integer> map = getDepthMap(file.getFullPath().toOSString());
		int depth = 0;
		
		for (Pair<Integer, Integer> pair : map.keyValuesView()) {
			if (pair.getOne() < offset) {
				depth = pair.getTwo();
			} else {
				break;
			}
		}
		return depth;
	}
}
