lexer grammar Ttcn3DocCommentLexer;

/*
 ******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************
*/

/*
 * Lexer grammar for documentation comments (ETSI ES 201 873-10 V4.5.1)
 *
 * author Miklos Magyari
 */
 
 @header {
 	import org.eclipse.core.resources.IFile;
 	import org.eclipse.core.resources.IMarker;
    import org.eclipse.titan.common.parsers.TITANMarker;
    import org.eclipse.titan.designer.AST.*;
    import org.eclipse.titan.designer.parsers.ParserMarkerSupport;
}

@members {
	private IFile actualFile;
			
	public void setActualFile(final IFile actualFile) {
		this.actualFile = actualFile;
	}
	
	public void createMarker(String msg) {
		TITANMarker warningMarker = new TITANMarker(msg, _interp.getLine(), _input.index(), _input.index(),
				IMarker.SEVERITY_WARNING, IMarker.PRIORITY_NORMAL);
	    ParserMarkerSupport.createWarningMarker(actualFile, warningMarker);
	}
}

BLOCK_BEGIN:        '/**' STAR*;
BLOCK_END:          '*/' ;

WS:	                [ \t]+ ;
AUTHOR:             '@author' WS;
CONFIG:             '@config' WS;
DESC:               '@desc' WS;
EXCEPTION:          '@exception' WS;
MEMBER:             '@member' WS;
PARAM:              '@param' WS;
PRIORITY:           '@priority' WS;
PURPOSE:            '@purpose' WS;
REFERENCE:          '@reference' WS;
REMARK:             '@remark' WS;
REQUIREMENT:        '@requirement' WS;
RETURN:             '@return' WS;
SEE:                '@see' WS;
SINCE:              '@since'WS;
STATUS:             '@status' WS;
URL:                '@url' WS;
VERDICT:            '@verdict' WS;
VERSION:            '@version' WS;
STAR:               '*' ;
NEWLINE:            (WS* STAR* WS*)? '\r'? '\n' (WS? (STAR { _input.LA(1) != '/'}?)+)?;

LINE:               WS* LINE_BEGIN STAR* WS* ;

IDENTIFIER:         [A-Za-z][A-Za-z0-9_]* ;

FREETEXT:           FREETEXTCHAR+ ;
fragment FREETEXTCHAR:  [\u0021-\u0029\u002b-\u002e\u0030-\u003f\u0041-\u007e\u00a1-\u00ac\u00ae-\u00ff]
                        | '@@'
                        | '*' { _input.LA(1) != '/'}? 
                        | '/' { _input.LA(2) != '*' || (_input.LA(1) != '*' && _input.LA(1) != '/') }?     
                        ;

fragment LINE_BEGIN:    '//*' STAR*;

BROKEN_AT:			'@' { /* createMarker("Character '@' should be escaped like '@@'"); */ } -> skip;
