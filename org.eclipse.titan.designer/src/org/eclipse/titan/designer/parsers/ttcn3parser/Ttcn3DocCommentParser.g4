parser grammar Ttcn3DocCommentParser;

/*
 ******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************
*/

/*
 * Parser grammar for documentation comments (ETSI ES 201 873-10 V4.5.1)
 *
 * author Miklos Magyari
 * author Adam Knapp
 */

options {
    tokenVocab=Ttcn3DocCommentLexer;
}

@header
{
    import java.lang.StringBuilder;
    import org.eclipse.core.resources.IFile;
    import org.eclipse.titan.designer.AST.*;
}

@members
{
private IFile parsedFile;
private Location commentLocation;

public void setFile(IFile file) {
    parsedFile = file;
}


public void setCommentLocation(Location location) {
    commentLocation = location;
}

/**
 * Create new location, which modified by the parser offset and line,
 * where the start and end token is the same
 * @param aToken the start and end token
 */
private Location getLocation(final Token aToken) {
	return getLocation(aToken, aToken);
}

/**
 * Create new location, which modified by the parser offset and line
 * @param aStartToken the 1st token, its line and start position will be used for the location
 *                  NOTE: start position is the column index of the tokens 1st character.
 *                        Column index starts with 0.
 * @param aEndToken the last token, its end position will be used for the location.
 *                  NOTE: end position is the column index after the token's last character.
 */
private Location getLocation(final Token aStartToken, final Token aEndToken) {
	final Token endToken = (aEndToken != null) ? aEndToken : aStartToken;
	return new Location(parsedFile, commentLocation.getLine(), commentLocation.getOffset() + aStartToken.getStartIndex(), commentLocation.getOffset() + endToken.getStopIndex() + 1);
}
}

pr_DocComment[DocumentComment documentComment]:
(   pr_BlockComment[documentComment]
|   pr_LineComments[documentComment]
)+;

pr_BlockComment[DocumentComment documentComment]:
    BLOCK_BEGIN 
    (   pr_Tag[documentComment]
    |   WS
    |   NEWLINE
    |   IDENTIFIER
    |   FREETEXT
    )*
    BLOCK_END
;

pr_LineComments[DocumentComment documentComment]:
    (   LINE
        ( pr_Tag[documentComment] )?
        NEWLINE?
    )+
;

pr_AuthorTag[DocumentComment documentComment]:
    AUTHOR
    txt = pr_FreeText { documentComment.addAuthor( $txt.text, getLocation($AUTHOR, $txt.endcol) ); }
;

pr_ConfigTag[DocumentComment documentComment]:
    CONFIG
    txt = pr_FreeText { documentComment.addConfig( $txt.text, getLocation($CONFIG, $txt.endcol) ); }
;

pr_DescTag[DocumentComment documentComment]:
    DESC
    txt = pr_FreeText { documentComment.addDesc( $txt.text, getLocation($DESC, $txt.endcol) ); }
;

pr_ExceptionTag[DocumentComment documentComment]
@init {
    String freetext = null;
    Token endcol = null;
}:
    EXCEPTION
    id = pr_Identifier { endcol = $id.stop; }
    WS?
    ( ft = pr_FreeText { 
        freetext = $ft.text;
        endcol = $ft.endcol;
    } )?
{
    documentComment.addException( $id.text, freetext, getLocation($EXCEPTION, endcol) );
};

pr_MemberTag[DocumentComment documentComment]
@init {
    String freetext = null;
    Token endcol = null;
}:
    MEMBER
    id = pr_Identifier { endcol = $id.stop; }
    WS?
    ( ft = pr_FreeText { 
        freetext = $ft.text; 
        endcol = $ft.endcol;
    } )?
{
    documentComment.addMember( $id.text, freetext, getLocation($MEMBER, endcol) );
};

pr_ParamTag[DocumentComment documentComment]
@init {
    String freetext = null;
    Token endcol = null;
}:
    PARAM
    id = pr_Identifier { endcol = $id.stop; }
    WS?
    ( ft = pr_FreeText { 
        freetext = $ft.text; 
        endcol = $ft.endcol;
    } )?
{
    documentComment.addParam( $id.text, freetext,  getLocation($PARAM, endcol) );
};

pr_PriorityTag[DocumentComment documentComment]:
    PRIORITY
    id = pr_Identifier { documentComment.addPriority( $id.text, getLocation($PRIORITY, $id.stop) ); }
;

pr_PurposeTag[DocumentComment documentComment]:
    PURPOSE
    txt = pr_FreeText { documentComment.addPurpose( $txt.text, getLocation($PURPOSE, $txt.endcol) ); }
;

pr_ReferenceTag[DocumentComment documentComment]:
    REFERENCE
    txt = pr_FreeText { documentComment.addReference( $txt.text, getLocation($REFERENCE, $txt.endcol) ); }
;

pr_RemarkTag[DocumentComment documentComment]:
    REMARK
    txt = pr_FreeText { documentComment.addRemark( $txt.text, getLocation($REMARK, $txt.endcol) ); }
;

pr_RequirementTag[DocumentComment documentComment]:
    REQUIREMENT
    txt = pr_FreeText { documentComment.addRequirement( $txt.text, getLocation($REQUIREMENT, $txt.endcol) ); }
;

pr_ReturnTag[DocumentComment documentComment]:
    RETURN
    txt = pr_FreeText { documentComment.addReturn( $txt.text, getLocation($RETURN, $txt.endcol) ); }
;

pr_SeeTag[DocumentComment documentComment]:
    SEE
    id = pr_Identifier { documentComment.addSee( $id.text, getLocation($SEE, $id.stop) ); }
;

pr_SinceTag[DocumentComment documentComment]:
    SINCE
    txt = pr_FreeText { documentComment.addSince( $txt.text, getLocation($SINCE, $txt.endcol) ); }
;

pr_StatusTag[DocumentComment documentComment]:
    STATUS
    txt = pr_FreeText { documentComment.addStatus( $txt.text, getLocation($STATUS, $txt.endcol) ); }
;

pr_UrlTag[DocumentComment documentComment]:
    URL
    txt = pr_FreeText { documentComment.addUrl( $txt.text, getLocation($URL, $txt.endcol) ); }
;

pr_VerdictTag[DocumentComment documentComment]
@init {
    String freetext = null;
    Token endcol = null;
}:
    VERDICT
    id = pr_Identifier { endcol = $id.stop; } 
    WS?
    ( ft = pr_FreeText {
        freetext = $ft.text;
        endcol = $ft.endcol;
    } )?
{
    documentComment.addVerdict( $id.text, freetext, getLocation($VERDICT, endcol) );
};

pr_VersionTag[DocumentComment documentComment]:
    VERSION
    txt = pr_FreeText { documentComment.addVersion( $txt.text, getLocation($VERSION, $txt.endcol) ); }
;

pr_Tag[DocumentComment documentComment]:
(   pr_AuthorTag[documentComment]
|   pr_ConfigTag[documentComment]
|   pr_DescTag[documentComment]
|   pr_ExceptionTag[documentComment]
|   pr_MemberTag[documentComment]
|   pr_ParamTag[documentComment]
|   pr_PriorityTag[documentComment]
|   pr_PurposeTag[documentComment]
|   pr_ReferenceTag[documentComment]
|   pr_RemarkTag[documentComment]
|   pr_RequirementTag[documentComment]
|   pr_ReturnTag[documentComment]
|   pr_SeeTag[documentComment]
|   pr_SinceTag[documentComment]
|   pr_StatusTag[documentComment]
|   pr_UrlTag[documentComment]
|   pr_VerdictTag[documentComment]
|   pr_VersionTag[documentComment]
);

pr_Identifier returns[String text]:
    IDENTIFIER { $text = $IDENTIFIER.getText(); }
;

pr_FreeText returns[String text, Token endcol]
@init {
    StringBuilder sb = new StringBuilder();
    $endcol = null;   
}:
    ( 
        WS?
        (   IDENTIFIER { sb.append( $IDENTIFIER.getText()); $endcol = $IDENTIFIER; } 
        |   FREETEXT { sb.append( $FREETEXT.getText()); $endcol = $FREETEXT; } 
        )
        ( WS { sb.append( $WS.getText() ); } )?
        ( NEWLINE { sb.append('\n'); })?
        ( WS { sb.append(' '); } )?
        LINE?
    )+
{
    $text = sb.toString();
}
;