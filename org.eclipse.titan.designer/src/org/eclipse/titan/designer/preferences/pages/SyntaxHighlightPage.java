/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.preferences.pages;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ColorFieldEditor;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.preference.PreferenceStore;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.titan.designer.Activator;
import org.eclipse.titan.designer.AST.TTCN3.definitions.ICommentable;
import org.eclipse.titan.designer.editors.ColorManager;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.titan.designer.preferences.PreferenceInitializer;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * This preference page hold the controls and functionality related to syntax
 * coloring.
 * 
 * @author Kristof Szabados
 */
public final class SyntaxHighlightPage extends PreferencePage implements IWorkbenchPreferencePage {
	private static final String TEMPLATE_CODE = 
			  "// TTCN-3 version of \"Hello, world!\"\n"
			+ "/**\n"
			+ " * @desc Demo module\n"
			+ " * @status deprecated\n"
			+ " * @url https://projects.eclipse.org/projects/tools.titan\n"
			+ " * @author AK, MM\n"
			+ " */\n"
			+ "module MyExample {\n"
			+ "type port PCOType message {\n"
			+ "  inout charstring;\n"
			+ "}\n\n"
			+ "type component MTCType {\n"
			+ "  port PCOType MyPCO_PT;\n"
			+ "}\n\n"
			+ "type record MyRecord {\n"
			+ "  integer x1,\n"
			+ "  charstring c2\n"
			+ "}\n\n"
			+ "type enumerated WorkDays { Monday, Tuesday, Wednesday, Thursday, Friday }\n\n"
			+ "modulepar integer modPar := 9;\n"
			+ "const integer c_integer := 33;\n\n"
			+ "public function myFunction(integer x, charstring y) return integer {\n"
			+ "  return x;\n"
			+ "}\n\n"
			+ "type class BaseClass {\n"
			+ "  private var integer f1;\n"
			+ "  const integer myConst;\n"
			+ "  public var MyRecord myRecord;\n"
			+ "}\n\n"
			+ "testcase tc_HelloW() runs on MTCType system MTCType {\n"
			+ "  timer TL_T := 15.0;\n"
			+ "  map(mtc:MyPCO_PT, system:MyPCO_PT);\n"
			+ "  MyPCO_PT.send(\"Hello, world!\");\n"
			+ "  TL_T.start;\n"
			+ "  alt {\n"
			+ "    [] MyPCO_PT.receive(\"Hello, TTCN-3!\") { TL_T.stop; setverdict(pass); }\n"
			+ "    [] TL_T.timeout { setverdict(inconc); }\n"
			+ "    [] MyPCO_PT.receive { TL_T.stop; setverdict(fail); }\n"
			+ "  }\n"
			+ "}\n\n"
			+ "control {\n"
			+ "  execute(tc_HelloW());\n"
			+ "}\n"
			+ "}\n";
	private static final String ENABLE_SEMANTIC_HIGHLIGHTING = "Enable semantic highlighting (EXPERIMENTAL)";

	private Composite pageComposite;
	private Composite upperHalfComposite;
	private Composite colorEditorsComposite;
	private ColorFieldEditor foregroundColorEditor;
	private ColorFieldEditor backgroundColorEditor;
	private BooleanFieldEditor useBackgroundColor;
	private BooleanFieldEditor isBold;
	private BooleanFieldEditor isItalic;
	private BooleanFieldEditor isStrikethrough;
	private BooleanFieldEditor isUnderline;
	private TreeViewer treeViewer;
	private StyledText textViewer;
	private SyntaxhighlightLabelProvider labelProvider;
	private Label previewLabel;
	private BooleanFieldEditor enableSemanticHighlighting;

	private PreferenceStore tempstore;
	private final Map<String, String> possiblyChangedPreferences = new HashMap<String, String>();

	private ColorManager colorManager = new ColorManager();
	private SyntaxHighlightColoringElement element = null;
	private SyntaxHighlightColoringGroup ttcn3SemanticGroup = null;

	private final ISelectionChangedListener treeListener = new ISelectionChangedListener() {
		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.ISelectionChangedListener#
		 * selectionChanged
		 * (org.eclipse.jface.viewers.SelectionChangedEvent)
		 */
		@Override
		public void selectionChanged(final SelectionChangedEvent event) {
			if (event.getSelection().isEmpty()) {
				return;
			}

			if (event.getSelection() instanceof IStructuredSelection) {
				final IStructuredSelection selection = (IStructuredSelection) event.getSelection();
				if (selection.size() == 1) {
					if (selection.getFirstElement() instanceof SyntaxHighlightColoringElement) {
						element = (SyntaxHighlightColoringElement) selection.getFirstElement();

						foregroundColorEditor.setEnabled(true, colorEditorsComposite);
						loadIntoTemp(element.getBasePreferenceKey() + PreferenceConstants.FOREGROUND);
						foregroundColorEditor.setPreferenceName(element.getBasePreferenceKey()
								+ PreferenceConstants.FOREGROUND);
						foregroundColorEditor.load();

						useBackgroundColor.setEnabled(true, colorEditorsComposite);
						loadIntoTemp(element.getBasePreferenceKey() + PreferenceConstants.USEBACKGROUNDCOLOR);
						useBackgroundColor.setPreferenceName(element.getBasePreferenceKey()
								+ PreferenceConstants.USEBACKGROUNDCOLOR);
						useBackgroundColor.load();

						backgroundColorEditor.setEnabled(useBackgroundColor.getBooleanValue(), colorEditorsComposite);
						loadIntoTemp(element.getBasePreferenceKey() + PreferenceConstants.BACKGROUND);
						backgroundColorEditor.setPreferenceName(element.getBasePreferenceKey()
								+ PreferenceConstants.BACKGROUND);
						backgroundColorEditor.load();

						isBold.setEnabled(true, colorEditorsComposite);
						loadIntoTemp(element.getBasePreferenceKey() + PreferenceConstants.BOLD);
						isBold.setPreferenceName(element.getBasePreferenceKey() + PreferenceConstants.BOLD);
						isBold.load();

						isItalic.setEnabled(true, colorEditorsComposite);
						loadIntoTemp(element.getBasePreferenceKey() + PreferenceConstants.ITALIC);
						isItalic.setPreferenceName(element.getBasePreferenceKey() + PreferenceConstants.ITALIC);
						isItalic.load();

						isStrikethrough.setEnabled(true, colorEditorsComposite);
						loadIntoTemp(element.getBasePreferenceKey() + PreferenceConstants.STRIKETHROUGH);
						isStrikethrough.setPreferenceName(element.getBasePreferenceKey() + PreferenceConstants.STRIKETHROUGH);
						isStrikethrough.load();

						isUnderline.setEnabled(true, colorEditorsComposite);
						loadIntoTemp(element.getBasePreferenceKey() + PreferenceConstants.UNDERLINE);
						isUnderline.setPreferenceName(element.getBasePreferenceKey() + PreferenceConstants.UNDERLINE);
						isUnderline.load();

						if (element.getWords() != null) {
							textViewer.setText(element.getWords());
							textViewer.setStyleRange(colorManager.createStyleRangeFromPreference(
									element.getBasePreferenceKey(), tempstore, 0, element.getWords().length()));
						} else {
							textViewer.setText("");
						}
						return;
					}
				}
			}

			foregroundColorEditor.setEnabled(false, colorEditorsComposite);
			backgroundColorEditor.setEnabled(false, colorEditorsComposite);
			useBackgroundColor.setEnabled(false, colorEditorsComposite);
			isBold.setEnabled(false, colorEditorsComposite);
			isItalic.setEnabled(false, colorEditorsComposite);
			isStrikethrough.setEnabled(false, colorEditorsComposite);
			isUnderline.setEnabled(false, colorEditorsComposite);
			textViewer.setText("");
		}
	};

	final class TempPreferenceInitializer extends PreferenceInitializer {

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.titan.designer.preferences.PreferenceInitializer
		 * #getPreference()
		 */
		@Override
		public IPreferenceStore getPreference() {
			return tempstore;
		}
	}

	private void loadIntoTemp(final String preferenceName) {
		if (!possiblyChangedPreferences.containsKey(preferenceName)) {
			tempstore.setValue(preferenceName, getPreferenceStore().getString(preferenceName));
		}
	}

	private void storeIntoFinal(final String preferenceName) {
		getPreferenceStore().setValue(preferenceName, tempstore.getString(preferenceName));
	}

	@Override
	public void init(final IWorkbench workbench) {
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		tempstore = new PreferenceStore();
		final TempPreferenceInitializer inittializer = new TempPreferenceInitializer();
		inittializer.initializeDefaultPreferences();
	}

	@Override
	public boolean performOk() {
		for (final String changedKey : possiblyChangedPreferences.keySet()) {
			storeIntoFinal(changedKey);
		}
		return super.performOk();
	}

	@Override
	protected void performDefaults() {
		final String[] preferenceNames = tempstore.preferenceNames();
		for (final String preferenceName : preferenceNames) {
			tempstore.setValue(preferenceName, tempstore.getDefaultString(preferenceName));
			possiblyChangedPreferences.put(preferenceName, null);
		}
		if (foregroundColorEditor != null) {
			foregroundColorEditor.loadDefault();
		}
		if (backgroundColorEditor != null) {
			backgroundColorEditor.loadDefault();
		}
		if (useBackgroundColor != null) {
			useBackgroundColor.loadDefault();
		}
		if (isBold != null) {
			isBold.loadDefault();
		}
		if (isItalic != null) {
			isItalic.loadDefault();
		}
		if (isStrikethrough != null) {
			isStrikethrough.loadDefault();
		}
		if (isUnderline != null) {
			isUnderline.loadDefault();
		}
		enableSemanticHighlighting.loadDefault();		
		super.performDefaults();
	}

	@Override
	public void dispose() {
		foregroundColorEditor.dispose();
		backgroundColorEditor.dispose();
		useBackgroundColor.dispose();
		isBold.dispose();
		isItalic.dispose();
		isStrikethrough.dispose();
		isUnderline.dispose();
		previewLabel.dispose();
		textViewer.dispose();
		labelProvider.dispose();
		colorEditorsComposite.dispose();
		upperHalfComposite.dispose();
		enableSemanticHighlighting.dispose();
		pageComposite.dispose();
		super.dispose();
	}

	private void createTreeViewer(final Composite parent) {
		final GridLayout treeLayout = new GridLayout();
		treeLayout.numColumns = 2;
		final GridData treeData = new GridData();
		treeData.horizontalAlignment = GridData.FILL;
		treeData.verticalAlignment = SWT.FILL;
		treeData.grabExcessHorizontalSpace = true;
		treeData.grabExcessVerticalSpace = true;

		treeViewer = new TreeViewer(parent);
		treeViewer.getControl().setLayoutData(treeData);
		treeViewer.setContentProvider(new SyntaxHighlightContentProvider());
		labelProvider = new SyntaxhighlightLabelProvider();
		treeViewer.setLabelProvider(labelProvider);

		treeViewer.setInput(initialInput());
		treeViewer.addSelectionChangedListener(treeListener);
	}

	private void createColorEditors(final Composite parent) {
		colorEditorsComposite = new Composite(parent, SWT.NONE);
		final GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		colorEditorsComposite.setLayout(layout);
		final GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		colorEditorsComposite.setLayoutData(gridData);

		loadIntoTemp(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.FOREGROUND);
		foregroundColorEditor = new ColorFieldEditor(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.FOREGROUND,
				"Color:", colorEditorsComposite);
		foregroundColorEditor.setEnabled(false, colorEditorsComposite);
		foregroundColorEditor.setPreferenceStore(tempstore);
		foregroundColorEditor.setPage(this);
		foregroundColorEditor.load();
		foregroundColorEditor.setPropertyChangeListener(new IPropertyChangeListener() {

			@Override
			public void propertyChange(final PropertyChangeEvent event) {
				possiblyChangedPreferences.put(foregroundColorEditor.getPreferenceName(), null);
				foregroundColorEditor.store();
				if (element != null) {
					textViewer.setStyleRange(colorManager.createStyleRangeFromPreference(
							element.getBasePreferenceKey(), tempstore, 0, element.getWords().length()));
				}
			}
		});

		loadIntoTemp(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.USEBACKGROUNDCOLOR);
		useBackgroundColor = new BooleanFieldEditor(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.USEBACKGROUNDCOLOR,
				"Enable background color", SWT.CHECK, colorEditorsComposite);
		useBackgroundColor.setEnabled(false, colorEditorsComposite);
		useBackgroundColor.setPreferenceStore(tempstore);
		useBackgroundColor.setPage(this);
		useBackgroundColor.load();
		useBackgroundColor.setPropertyChangeListener(new IPropertyChangeListener() {

			@Override
			public void propertyChange(final PropertyChangeEvent event) {
				possiblyChangedPreferences.put(useBackgroundColor.getPreferenceName(), null);
				useBackgroundColor.store();
				backgroundColorEditor.setEnabled(useBackgroundColor.getBooleanValue(), colorEditorsComposite);
				textViewer.setStyleRange(colorManager.createStyleRangeFromPreference(
						element.getBasePreferenceKey(), tempstore, 0, element.getWords().length()));
			}
		});

		loadIntoTemp(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.BACKGROUND);
		backgroundColorEditor = new ColorFieldEditor(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.BACKGROUND,
				"Background color:", colorEditorsComposite);
		backgroundColorEditor.setEnabled(false, colorEditorsComposite);
		backgroundColorEditor.setPreferenceStore(tempstore);
		backgroundColorEditor.setPage(this);
		backgroundColorEditor.load();
		backgroundColorEditor.setPropertyChangeListener(new IPropertyChangeListener() {

			@Override
			public void propertyChange(final PropertyChangeEvent event) {
				possiblyChangedPreferences.put(backgroundColorEditor.getPreferenceName(), null);
				backgroundColorEditor.store();
				if (element != null) {
					textViewer.setStyleRange(colorManager.createStyleRangeFromPreference(
							element.getBasePreferenceKey(), tempstore, 0, element.getWords().length()));
				}
			}
		});

		loadIntoTemp(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.BOLD);
		isBold = new BooleanFieldEditor(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.BOLD, "Bold", SWT.CHECK,
				colorEditorsComposite);
		isBold.setEnabled(false, colorEditorsComposite);
		isBold.setPreferenceStore(tempstore);
		isBold.setPage(this);
		isBold.load();
		isBold.setPropertyChangeListener(new IPropertyChangeListener() {

			@Override
			public void propertyChange(final PropertyChangeEvent event) {
				possiblyChangedPreferences.put(isBold.getPreferenceName(), null);
				isBold.store();
				if (element != null) {
					textViewer.setStyleRange(colorManager.createStyleRangeFromPreference(
							element.getBasePreferenceKey(), tempstore, 0, element.getWords().length()));
				}
			}
		});

		loadIntoTemp(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.ITALIC);
		isItalic = new BooleanFieldEditor(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.ITALIC, "Italic", SWT.CHECK,
				colorEditorsComposite);
		isItalic.setEnabled(false, colorEditorsComposite);
		isItalic.setPreferenceStore(tempstore);
		isItalic.setPage(this);
		isItalic.load();
		isItalic.setPropertyChangeListener(new IPropertyChangeListener() {

			@Override
			public void propertyChange(final PropertyChangeEvent event) {
				possiblyChangedPreferences.put(isItalic.getPreferenceName(), null);
				isItalic.store();
				if (element != null) {
					textViewer.setStyleRange(colorManager.createStyleRangeFromPreference(
							element.getBasePreferenceKey(), tempstore, 0, element.getWords().length()));
				}
			}
		});

		loadIntoTemp(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.STRIKETHROUGH);
		isStrikethrough = new BooleanFieldEditor(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.STRIKETHROUGH, "Striketrough", SWT.CHECK,
				colorEditorsComposite);
		isStrikethrough.setEnabled(false, colorEditorsComposite);
		isStrikethrough.setPreferenceStore(tempstore);
		isStrikethrough.setPage(this);
		isStrikethrough.load();
		isStrikethrough.setPropertyChangeListener(new IPropertyChangeListener() {

			@Override
			public void propertyChange(final PropertyChangeEvent event) {
				possiblyChangedPreferences.put(isStrikethrough.getPreferenceName(), null);
				isStrikethrough.store();
				if (element != null) {
					textViewer.setStyleRange(colorManager.createStyleRangeFromPreference(
							element.getBasePreferenceKey(), tempstore, 0, element.getWords().length()));
				}
			}
		});

		loadIntoTemp(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.UNDERLINE);
		isUnderline = new BooleanFieldEditor(PreferenceConstants.COLOR_TTCN3_KEYWORDS + PreferenceConstants.UNDERLINE, "Underline", SWT.CHECK,
				colorEditorsComposite);
		isUnderline.setEnabled(false, colorEditorsComposite);
		isUnderline.setPreferenceStore(tempstore);
		isUnderline.setPage(this);
		isUnderline.load();
		isUnderline.setPropertyChangeListener(new IPropertyChangeListener() {

			@Override
			public void propertyChange(final PropertyChangeEvent event) {
				possiblyChangedPreferences.put(isUnderline.getPreferenceName(), null);
				isUnderline.store();
				if (element != null) {
					textViewer.setStyleRange(colorManager.createStyleRangeFromPreference(
							element.getBasePreferenceKey(), tempstore, 0, element.getWords().length()));
				}
			}
		});

		GridData wordsGridData = new GridData();
		wordsGridData.horizontalAlignment = GridData.FILL;
		wordsGridData.verticalAlignment = GridData.FILL;
		wordsGridData.horizontalSpan = 2;

		previewLabel = new Label(colorEditorsComposite, SWT.SINGLE);
		previewLabel.setText("Preview: ");
		previewLabel.setVisible(true);
		previewLabel.setLayoutData(wordsGridData);

		wordsGridData = new GridData();
		wordsGridData.horizontalAlignment = GridData.FILL;
		wordsGridData.verticalAlignment = GridData.FILL;
		wordsGridData.horizontalSpan = 2;
		wordsGridData.grabExcessHorizontalSpace = true;
		wordsGridData.grabExcessVerticalSpace = true;

		textViewer = new StyledText(colorEditorsComposite, SWT.V_SCROLL | SWT.BORDER | SWT.MULTI | SWT.WRAP | SWT.READ_ONLY);
		textViewer.setEditable(false);
		textViewer.setLayoutData(wordsGridData);
		textViewer.setBackground(getShell().getBackground());
		textViewer.setFont(getFont());
	}

	private void createSemanticHighlightingCheckbox(final Composite parent) {
		loadIntoTemp(PreferenceConstants.ENABLE_SEMANTIC_HIGHLIGHTING);
		enableSemanticHighlighting = new BooleanFieldEditor(PreferenceConstants.ENABLE_SEMANTIC_HIGHLIGHTING,
				ENABLE_SEMANTIC_HIGHLIGHTING, SWT.CHECK, parent);
		enableSemanticHighlighting.setEnabled(true, parent);
		enableSemanticHighlighting.setPreferenceStore(tempstore);
		enableSemanticHighlighting.setPage(this);
		enableSemanticHighlighting.load();
		enableSemanticHighlighting.setPropertyChangeListener(new IPropertyChangeListener() {
			@Override
			public void propertyChange(final PropertyChangeEvent event) {
				possiblyChangedPreferences.put(enableSemanticHighlighting.getPreferenceName(), null);
				enableSemanticHighlighting.store();
				if (ttcn3SemanticGroup != null) {
					ttcn3SemanticGroup.setEnabled(enableSemanticHighlighting.getBooleanValue());
					treeViewer.refresh();
				}
			}
		});
	}

	private void createUpperHalf(final Composite parent) {
		upperHalfComposite = new Composite(parent, SWT.NONE);
		final GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		upperHalfComposite.setLayout(layout);
		final GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		upperHalfComposite.setLayoutData(gridData);

		createTreeViewer(upperHalfComposite);
		createColorEditors(upperHalfComposite);
	}

	@Override
	protected Control createContents(final Composite parent) {
		pageComposite = new Composite(parent, SWT.NONE);
		final GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		pageComposite.setLayout(layout);
		final GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		pageComposite.setLayoutData(gridData);

		createSemanticHighlightingCheckbox(pageComposite);
		createUpperHalf(pageComposite);

		return pageComposite;
	}

	private SyntaxHighlightColoringGroup initialInput() {
		final SyntaxHighlightColoringGroup root = new SyntaxHighlightColoringGroup("root");

		final SyntaxHighlightColoringGroup generalGroup = new SyntaxHighlightColoringGroup("General");
		generalGroup.add(new SyntaxHighlightColoringElement("Plain text", PreferenceConstants.COLOR_NORMAL_TEXT, "Example text"));
		generalGroup.add(new SyntaxHighlightColoringElement("Comments", PreferenceConstants.COLOR_COMMENTS, "/* Example comment */"));
		generalGroup.add(new SyntaxHighlightColoringElement("Documentation comment tags", PreferenceConstants.COLOR_COMMENT_TAG,
				ICommentable.DOC_COMMENT_TAGS));
		generalGroup.add(new SyntaxHighlightColoringElement("HTML tags in document comments", PreferenceConstants.COLOR_HTML_TAG, 
				"<li>Example item</li>"));
		generalGroup.add(new SyntaxHighlightColoringElement("Strings", PreferenceConstants.COLOR_STRINGS, "\" Example string \""));
		root.add(generalGroup);

		final SyntaxHighlightColoringGroup asn1Group = new SyntaxHighlightColoringGroup("ASN.1 specific");
		asn1Group.add(new SyntaxHighlightColoringElement("Keywords", PreferenceConstants.COLOR_ASN1_KEYWORDS,
				org.eclipse.titan.designer.editors.asn1editor.CodeScanner.KEYWORDS));
		asn1Group.add(new SyntaxHighlightColoringElement("CMIP verbs", PreferenceConstants.COLOR_CMIP_VERB,
				org.eclipse.titan.designer.editors.asn1editor.CodeScanner.VERBS));
		asn1Group.add(new SyntaxHighlightColoringElement("compare types", PreferenceConstants.COLOR_COMPARE_TYPE,
				org.eclipse.titan.designer.editors.asn1editor.CodeScanner.COMPARE_TYPES));
		asn1Group.add(new SyntaxHighlightColoringElement("Status", PreferenceConstants.COLOR_STATUS,
				org.eclipse.titan.designer.editors.asn1editor.CodeScanner.STATUS_TYPE));
		asn1Group.add(new SyntaxHighlightColoringElement("Tags", PreferenceConstants.COLOR_TAG,
				org.eclipse.titan.designer.editors.asn1editor.CodeScanner.TAGS));
		asn1Group.add(new SyntaxHighlightColoringElement("Storage", PreferenceConstants.COLOR_STORAGE,
				org.eclipse.titan.designer.editors.asn1editor.CodeScanner.STORAGE));
		asn1Group.add(new SyntaxHighlightColoringElement("Modifier", PreferenceConstants.COLOR_MODIFIER,
				org.eclipse.titan.designer.editors.asn1editor.CodeScanner.MODIFIER));
		asn1Group.add(new SyntaxHighlightColoringElement("Access types", PreferenceConstants.COLOR_ACCESS_TYPE,
				org.eclipse.titan.designer.editors.asn1editor.CodeScanner.ACCESS_TYPE));
		root.add(asn1Group);

		final SyntaxHighlightColoringGroup configGroup = new SyntaxHighlightColoringGroup("Configuration specific");
		configGroup.add(new SyntaxHighlightColoringElement("Keywords", PreferenceConstants.COLOR_CONFIG_KEYWORDS,
				org.eclipse.titan.designer.editors.configeditor.CodeScanner.KEYWORDS));
		configGroup.add(new SyntaxHighlightColoringElement("Section title", PreferenceConstants.COLOR_SECTION_TITLE,
				org.eclipse.titan.designer.editors.configeditor.CodeScanner.SECTION_TITLES));
		configGroup.add(new SyntaxHighlightColoringElement("File and control mask options",
				PreferenceConstants.COLOR_FILE_AND_CONTROL_MASK_OPTIONS,
				org.eclipse.titan.designer.editors.configeditor.CodeScanner.MASK_OPTIONS));
		configGroup.add(new SyntaxHighlightColoringElement("External command types", PreferenceConstants.COLOR_EXTERNAL_COMMAND_TYPES,
				org.eclipse.titan.designer.editors.configeditor.CodeScanner.EXTERNAL_COMMAND_TYPES));
		root.add(configGroup);

		final SyntaxHighlightColoringGroup ttcn3Group = new SyntaxHighlightColoringGroup("TTCN-3 specific");
		ttcn3Group.add(new SyntaxHighlightColoringElement("Keywords", PreferenceConstants.COLOR_TTCN3_KEYWORDS,
				org.eclipse.titan.designer.editors.ttcn3editor.CodeScanner.KEYWORDS));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Preprocessor", PreferenceConstants.COLOR_PREPROCESSOR, "Example #include"));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Visibility modifiers", PreferenceConstants.COLOR_VISIBILITY_OP,
				org.eclipse.titan.designer.editors.ttcn3editor.CodeScanner.VISIBILITY_MODIFIERS));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Template match", PreferenceConstants.COLOR_TEMPLATE_MATCH,
				org.eclipse.titan.designer.editors.ttcn3editor.CodeScanner.TEMPLATE_MATCH));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Type", PreferenceConstants.COLOR_TYPE,
				org.eclipse.titan.designer.editors.ttcn3editor.CodeScanner.TYPES));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Timer operators", PreferenceConstants.COLOR_TIMER_OP,
				org.eclipse.titan.designer.editors.ttcn3editor.CodeScanner.TIMER_OPERATIONS));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Port operators", PreferenceConstants.COLOR_PORT_OP,
				org.eclipse.titan.designer.editors.ttcn3editor.CodeScanner.PORT_OPERATIONS));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Config operators", PreferenceConstants.COLOR_CONFIG_OP,
				org.eclipse.titan.designer.editors.ttcn3editor.CodeScanner.CONFIGURATION_OPERATIONS));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Verdict operators", PreferenceConstants.COLOR_VERDICT_OP,
				org.eclipse.titan.designer.editors.ttcn3editor.CodeScanner.VERDICT_OPERATIONS));
		ttcn3Group.add(new SyntaxHighlightColoringElement("System under test related operators", PreferenceConstants.COLOR_SUT_OP,
				org.eclipse.titan.designer.editors.ttcn3editor.CodeScanner.SUT_OPERATION));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Function operators", PreferenceConstants.COLOR_FUNCTION_OP,
				org.eclipse.titan.designer.editors.ttcn3editor.CodeScanner.FUNCTION_OPERATIONS));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Predefined operators", PreferenceConstants.COLOR_PREDEFINED_OP,
				org.eclipse.titan.designer.editors.ttcn3editor.CodeScanner.PREDEFINED_OPERATIONS));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Boolean consts", PreferenceConstants.COLOR_BOOLEAN_CONST,
				org.eclipse.titan.designer.editors.ttcn3editor.CodeScanner.BOOLEAN_CONSTANTS));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Verdict consts", PreferenceConstants.COLOR_TTCN3_VERDICT_CONST,
				org.eclipse.titan.designer.editors.ttcn3editor.CodeScanner.VERDICT_CONSTANT));
		ttcn3Group.add(new SyntaxHighlightColoringElement("Other consts", PreferenceConstants.COLOR_OTHER_CONST,
				org.eclipse.titan.designer.editors.ttcn3editor.CodeScanner.OTHER_CONSTANT));
		root.add(ttcn3Group);

		ttcn3SemanticGroup = new SyntaxHighlightColoringGroup("TTCN-3 semantic specific");
		//ttcn3SemanticGroup.add(new SyntaxHighlightColoringElement("Component types",
		//		PreferenceConstants.COLOR_TTCN3_KEYWORDS, TEMPLATE_CODE));
		ttcn3SemanticGroup.add(new SyntaxHighlightColoringElement("Constants",
				PreferenceConstants.COLOR_AST_CONSTANT,	TEMPLATE_CODE));
		ttcn3SemanticGroup.add(new SyntaxHighlightColoringElement("Deprecated elements",
				PreferenceConstants.COLOR_AST_DEPRECATED, TEMPLATE_CODE));
		//ttcn3SemanticGroup.add(new SyntaxHighlightColoringElement("Module parameters",
		//		PreferenceConstants.COLOR_TTCN3_KEYWORDS, TEMPLATE_CODE));
		ttcn3SemanticGroup.add(new SyntaxHighlightColoringElement("Type definitions",
				PreferenceConstants.COLOR_AST_DEFTYPE, TEMPLATE_CODE));
		ttcn3SemanticGroup.add(new SyntaxHighlightColoringElement("Variables",
				PreferenceConstants.COLOR_AST_VARIABLE, TEMPLATE_CODE));
		ttcn3SemanticGroup.setEnabled(enableSemanticHighlighting.getBooleanValue());
		root.add(ttcn3SemanticGroup);

		return root;
	}
}
