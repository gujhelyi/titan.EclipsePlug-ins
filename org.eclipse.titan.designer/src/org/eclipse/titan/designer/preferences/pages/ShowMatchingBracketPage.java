/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.preferences.pages;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ColorFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.titan.designer.Activator;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * This preference page holds the controls and functionality to set the matching
 * bracket feature related options.
 * 
 * @author Kristof Szabados
 * @author Miklos Magyari
 */
public final class ShowMatchingBracketPage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	static final String DESCRIPTION = "Preferences for the editor's matching brackets";
	static final String HIGHLIGHT_MATCHING_BRACKETS = "Highligh matching brackets";
	static final String MATCHING_BRACKETS_COLORING = "Enable colorizing of matching brackets (EXPERIMENTAL)";
	static final String COLOR = "color:";
	
	private boolean isEnabled;
	private ColorFieldEditor matchingBracketColor;
	private BooleanFieldEditor enableBracketColoring;
	private ColorFieldEditor bracketColor0;
	private ColorFieldEditor bracketColor1;
	private ColorFieldEditor bracketColor2;
	private ColorFieldEditor bracketColor3;
	private ColorFieldEditor bracketColor4;
	
	public ShowMatchingBracketPage() {
		super(GRID);
	}

	@Override
	public void init(final IWorkbench workbench) {
		setDescription(DESCRIPTION);
	}

	@Override
	protected void createFieldEditors() {
		final BooleanFieldEditor enableMatchingBrackets = new BooleanFieldEditor(PreferenceConstants.MATCHING_BRACKET_ENABLED,
				HIGHLIGHT_MATCHING_BRACKETS, getFieldEditorParent());
		addField(enableMatchingBrackets);

		matchingBracketColor = new ColorFieldEditor(PreferenceConstants.COLOR_MATCHING_BRACKET, COLOR,
				getFieldEditorParent());
		addField(matchingBracketColor);
		enableBracketColoring = new BooleanFieldEditor(PreferenceConstants.BRACKET_COLORING_ENABLED, MATCHING_BRACKETS_COLORING, 
				getFieldEditorParent());
		addField(enableBracketColoring);
		
		bracketColor0 = new ColorFieldEditor(PreferenceConstants.BRACKET_COLOR_FOREGROUND_0, "Bracket color level 1",
				getFieldEditorParent());
		addField(bracketColor0);
		bracketColor1 = new ColorFieldEditor(PreferenceConstants.BRACKET_COLOR_FOREGROUND_1, "Bracket color level 2",
				getFieldEditorParent());
		addField(bracketColor1);
		bracketColor2 = new ColorFieldEditor(PreferenceConstants.BRACKET_COLOR_FOREGROUND_2, "Bracket color level 2",
				getFieldEditorParent());
		addField(bracketColor2);
		bracketColor3 = new ColorFieldEditor(PreferenceConstants.BRACKET_COLOR_FOREGROUND_3, "Bracket color level 2",
				getFieldEditorParent());
		addField(bracketColor3);
		bracketColor4 = new ColorFieldEditor(PreferenceConstants.BRACKET_COLOR_FOREGROUND_4, "Bracket color level 2",
				getFieldEditorParent());
		addField(bracketColor4);
	}

	@Override
	protected IPreferenceStore doGetPreferenceStore() {
		return Activator.getDefault().getPreferenceStore();
	}
	
	@Override
    public boolean performOk() {
		getPreferenceStore().setValue(PreferenceConstants.BRACKET_COLORING_ENABLED, isEnabled);
        return super.performOk();
    }
	
	@Override
	public void dispose() {
		matchingBracketColor.dispose();
		enableBracketColoring.dispose();
		bracketColor0.dispose();
		bracketColor1.dispose();
		bracketColor2.dispose();
		bracketColor3.dispose();
		bracketColor4.dispose();
	}
}
