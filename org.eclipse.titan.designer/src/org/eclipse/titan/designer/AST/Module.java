/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.collections.impl.map.mutable.primitive.IntObjectHashMap;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.IPreferencesService;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IPositionUpdater;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.titan.designer.Activator;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Type;
import org.eclipse.titan.designer.compiler.JavaGenData;
import org.eclipse.titan.designer.declarationsearch.Declaration;
import org.eclipse.titan.designer.editors.AstSyntaxHighlightTokens;
import org.eclipse.titan.designer.editors.ColorManager;
import org.eclipse.titan.designer.editors.DocumentTracker;
import org.eclipse.titan.designer.editors.controls.PeekSource;
import org.eclipse.titan.designer.editors.controls.Ttcn3HoverContent;
import org.eclipse.titan.designer.editors.ttcn3editor.TTCN3Editor;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.ProjectStructureDataCollector;
import org.eclipse.titan.designer.preferences.PreferenceConstants;
import org.eclipse.titan.designer.productUtilities.ProductConstants;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.texteditor.AbstractDecoratedTextEditor;

/**
 * This class represents general Modules, on which both the TTCN-3 and the ASN.1
 * modules are built.
 *
 * @author Kristof Szabados
 * @author Miklos Magyari
 * */
public abstract class Module extends Scope implements IOutlineElement, ILocateableNode, IReferencingElement, IPositionUpdater {
	public enum module_type {
		/** ASN.1 module */
		ASN_MODULE,
		/** TTCN-3 module */
		TTCN3_MODULE
	}

	public enum SyntaxDecoration {
		Bracket0,						/* different levels of brackets */
		Bracket1,
		Bracket2,
		Bracket3,
		Bracket4,
		Bracket5,
		Bracket6,
		Bracket7,
		Bracket8,
		Bracket9,
		Constant, 						/* TTCN3 constant */
		DefType, 						/* own type defined with the 'type' keyword */
		Deprecated,						/* object marked as 'deprecated' in the status field of document comment */
		Variable,						/* TTCN3 variable */
	}

	/** Map of style tokens associated with special AST-based syntax elements */ 
	private static Map<SyntaxDecoration, IToken> tokenMap = new HashMap<>();

	public static final String MODULE = "module";

	protected IProject project;
	protected Identifier identifier;
	protected String name;

	protected Location location;

	/**
	 * Tells whether the module can be skipped from semantic checking in
	 * this semantic check cycle or not.
	 */
	private boolean isSkippedFromSemanticChecking = false;

	protected CompilationTimeStamp lastCompilationTimeStamp;
	protected CompilationTimeStamp lastImportCheckTimeStamp;

	private TTCN3Editor editor;

	static {
		final IPreferencesService prefs = Platform.getPreferencesService();
		final boolean isSemanticHighlightingEnabled = prefs.getBoolean(ProductConstants.PRODUCT_ID_DESIGNER,
				PreferenceConstants.ENABLE_SEMANTIC_HIGHLIGHTING, false, null);
		if (isSemanticHighlightingEnabled) {
			addTokens();
		}

		Activator.getDefault().getPreferenceStore().addPropertyChangeListener((event) -> {
			final String property = event.getProperty();
			if (PreferenceConstants.ENABLE_SEMANTIC_HIGHLIGHTING.equals(property)) {
				final boolean isEnabled = prefs.getBoolean(ProductConstants.PRODUCT_ID_DESIGNER,
						PreferenceConstants.ENABLE_SEMANTIC_HIGHLIGHTING, false, null);
				if (isEnabled) {
					addTokens();
				}
			}
		});
	}

	private static void addTokens() {
		ColorManager colorManager = new ColorManager();
		tokenMap.put(SyntaxDecoration.Constant, colorManager.createTokenFromPreference(PreferenceConstants.COLOR_AST_CONSTANT));
		tokenMap.put(SyntaxDecoration.DefType, colorManager.createTokenFromPreference(PreferenceConstants.COLOR_AST_DEFTYPE));
		tokenMap.put(SyntaxDecoration.Deprecated, colorManager.createTokenFromPreference(PreferenceConstants.COLOR_AST_DEPRECATED));
		tokenMap.put(SyntaxDecoration.Variable, colorManager.createTokenFromPreference(PreferenceConstants.COLOR_AST_VARIABLE));
	}

	public Module(final Identifier identifier, final IProject project) {
		this.project = project;
		this.identifier = identifier;
		name = (identifier == null) ? null : this.identifier.getName();

		scopeName = (identifier == null) ? "unknown module" : identifier.getDisplayName();
		setScopeMacroName((identifier == null) ? "unknown module" : identifier.getDisplayName());

		final AbstractDecoratedTextEditor editor = DocumentTracker.get(this);
		if (editor != null && editor instanceof TTCN3Editor) {
			this.editor = (TTCN3Editor)editor;
		}
	}

	public abstract module_type getModuletype();

	/**
	 * Returns the name of the module.
	 *
	 * @return the name of the module
	 * */
	public String getName() {
		return name;
	}

	public final IProject getProject() {
		return project;
	}

	/**
	 * Returns the timestamp of the last importation check.
	 * <p>
	 * The import hierarchy check is a pre step of real check.
	 *
	 * @return the timestamp of the last importation check.
	 * */
	public final CompilationTimeStamp getLastImportationCheckTimeStamp() {
		return lastImportCheckTimeStamp;
	}

	/**
	 * @return the timestamp of the last importation check.
	 * */
	public final CompilationTimeStamp getLastCompilationTimeStamp() {
		return lastCompilationTimeStamp;
	}

	/** used by the incremental processing to signal if the module can be the root of a change */
	private boolean canBeCheckRoot = true;

	/**
	 * returns true if the assignment is the root of a change.
	 * */
	public final boolean isCheckRoot() {
		return canBeCheckRoot;
	}

	/**
	 * Signals that the assignment can serve as a change root for the incremental analysis.
	 * */
	public final void checkRoot() {
		canBeCheckRoot = true;
	}

	/**
	 * Signals that the assignment can not serve as a change root for the incremental analysis.
	 * */
	public final void notCheckRoot() {
		canBeCheckRoot = false;
	}

	/**
	 * Sets whether this module can be skipped from semantic checking.
	 *
	 * @param state
	 *                the value telling if the module can be skipped from
	 *                semantic checking
	 * */
	public final void setSkippedFromSemanticChecking(final boolean state) {
		isSkippedFromSemanticChecking = state;
	}

	/**
	 * Checks weather this module can be skipped from semantic checking.
	 *
	 * @return true if the semantic checking of the module can be skipped,
	 *         false otherwise.
	 * */
	public final boolean getSkippedFromSemanticChecking() {
		return isSkippedFromSemanticChecking;
	}

	@Override
	/** {@inheritDoc} */
	public Identifier getIdentifier() {
		return identifier;
	}

	@Override
	/** {@inheritDoc} */
	public Location getLocation() {
		return location;
	}

	@Override
	/** {@inheritDoc} */
	public void setLocation(final Location location) {
		this.location = location;
	}

	@Override
	/** {@inheritDoc} */
	public Module getModuleScope() {
		return this;
	}

	@Override
	/** {@inheritDoc} */
	public Module getModuleScopeGen() {
		return this;
	}

	/**
	 * @return the list of definitions declared in this module.
	 * */
	public abstract Assignments getAssignments();

	public abstract Def_Type getAnytype();

	/**
	 * Does the semantic checking of this module.
	 * <p>
	 * <ul>
	 * <li>the definitions of this module are checked one-by-one
	 * </ul>
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle
	 * */
	public abstract void check(final CompilationTimeStamp timestamp);

	/**
	 * Checks the import hierarchies of this module (and the ones imported
	 * here recursively).
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 * @param referenceChain
	 *                a chain of references used to find circularly imported
	 *                modules.
	 * @param moduleStack
	 *                the stack of modules visited so far, from the starting
	 *                point.
	 * */
	public abstract void checkImports(final CompilationTimeStamp timestamp, final ModuleImportationChain referenceChain,
			final List<Module> moduleStack);

	/**
	 * Collects the imported modules into a list. The list shall always
	 * exist even if being empty, and shall have a null object when
	 * non-existing modules are imported.
	 *
	 * @return the list of modules imported.
	 * */
	public abstract List<Module> getImportedModules();

	/**
	 * @return Whether any of the modules imported has changed to an other
	 *         one since the last importation check.
	 * */
	public abstract boolean hasUnhandledImportChanges();

	/**
	 * Checks if there is an assignment imported to the actual module with
	 * the provided identifier.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 * @param identifier
	 *                the identifier to search for.
	 *
	 * @return true if there is an assignment imported with the provided
	 *         name, false otherwise.
	 * */
	public abstract boolean hasImportedAssignmentWithID(final CompilationTimeStamp timestamp, final Identifier identifier);

	/**
	 * Checks whether an assignment exist in the actual module, whether it
	 * is visible or not. If it exists and is visible it is returned.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 * @param moduleId
	 *                the identifier of the module, where the assignment
	 *                should be imported into.
	 * @param reference
	 *                the reference of the assignment to import.
	 *
	 * @return the assignment, or null in case of errors.
	 * */
	public abstract Assignment importAssignment(final CompilationTimeStamp timestamp, final Identifier moduleId, final Reference reference);

	/**
	 * Checks whether the assignment in this module is visible in the
	 * provided module or not. Used by declaration, and completion proposal
	 * searching.
	 *
	 * @param timestamp
	 *                the timestamp of the actual semantic check cycle.
	 * @param moduleId
	 *                the identifier of the module, against which the
	 *                visibility of the assignment is checked.
	 * @param assignment
	 *                the assignment to check.
	 *
	 * @return true if it is visible, false otherwise.
	 * */
	public abstract boolean isVisible(final CompilationTimeStamp timestamp, final Identifier moduleId, final Assignment assignment);

	/**
	 * Checks properties, that can only be checked after the semantic check
	 * was completely run.
	 */
	public abstract void postCheck();

	public String chainedDescription() {
		if (identifier != null) {
			return new StringBuilder("`").append(identifier.getDisplayName()).append('\'').toString();
		}

		return toString();
	}

	@Override
	/** {@inheritDoc} */
	public int category() {
		return 0;
	}

	@Override
	/** {@inheritDoc} */
	public String getOutlineText() {
		return "";
	}

	public abstract void extractStructuralInformation(ProjectStructureDataCollector collector);

	/**
	 * Called by accept(), objects have to call accept() of their members in
	 * this function
	 *
	 * @param v
	 *                the visitor object
	 * @return false to abort, will be returned by accept()
	 */
	protected boolean memberAccept(final ASTVisitor v) {
		if (identifier != null && !identifier.accept(v)) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public boolean accept(final ASTVisitor v) {
		switch (v.visit(this)) {
		case ASTVisitor.V_ABORT:
			return false;
		case ASTVisitor.V_SKIP:
			return true;
		}
		if (!memberAccept(v)) {
			return false;
		}
		if (v.leave(this) == ASTVisitor.V_ABORT) {
			return false;
		}
		return true;
	}

	@Override
	/** {@inheritDoc} */
	public Declaration getDeclaration() {
		return Declaration.createInstance(this);
	}

	/**
	 * Tells if the modules should be generated or not.
	 * <p>
	 * This is an overestimation helping with pre-filtering. The actual code
	 * generated might still be the one as before.
	 *
	 * @return {@code true} if the module should be generated, {@code false}
	 *         otherwise.
	 * */
	public abstract boolean shouldBeGenerated();

	/**
	 * Add generated java code for this module.
	 *
	 * @param aData the generated java code with other info.
	 */
	public abstract void generateCode( final JavaGenData aData );

	@Override
	/** {@inheritDoc} */
	public Ttcn3HoverContent getHoverContent(IEditorPart editor) {
		hoverContent = new Ttcn3HoverContent();
		if (editor != null) {
			PeekSource.addStyledSource(PeekSource.getPeekSource(editor, getLocation()), hoverContent);
		}
		return hoverContent;
	}

	/** Sets a special syntax token at the given location */
	public void addSyntaxDecoration(int offset, String text, SyntaxDecoration decoration) {
		final IntObjectHashMap<IToken> syntaxMap = AstSyntaxHighlightTokens.getOffsetMap(identifier.getDisplayName());
		if (syntaxMap == null) {
			return;
		}
		IToken token = tokenMap.get(decoration);
		syntaxMap.asSynchronized().put(offset, token);
	}

	public void removeSyntaxDecoration(int offset) {
		final IntObjectHashMap<IToken> syntaxMap = AstSyntaxHighlightTokens.getOffsetMap(identifier.getDisplayName());
		if (syntaxMap == null) {
			return;
		}
		syntaxMap.asSynchronized().remove(offset);
	}
	
	public void removeSpecificSyntaxDecoration(int offset, SyntaxDecoration decoration) {
		final IntObjectHashMap<IToken> syntaxMap = AstSyntaxHighlightTokens.getOffsetMap(identifier.getDisplayName());
		if (syntaxMap == null) {
			return;
		}
		final IToken token = tokenMap.get(decoration);
		if (syntaxMap.get(offset) == token) {
			syntaxMap.asSynchronized().remove(offset);
		}
	}

	/**
	 * Returns the special syntax token at a given offset, if exists 
	 * @param offset
	 * @return
	 */
	public IToken getSyntaxToken(int offset) {
		final IntObjectHashMap<IToken> syntaxMap = AstSyntaxHighlightTokens.getOffsetMap(identifier.getDisplayName());
		if (syntaxMap == null) {
			return null;
		}
		return syntaxMap.get(offset);
	}

	@Override
	public synchronized void update(DocumentEvent event) {
		final IntObjectHashMap<IToken> syntaxMap = AstSyntaxHighlightTokens.getOffsetMap(identifier.getDisplayName());
		if (syntaxMap == null) {
			return;
		}

		int change = event.getText().length() - event.fLength; 

		IntObjectHashMap<IToken> tempMap = new IntObjectHashMap<>();
		syntaxMap.forEachKeyValue((int key, IToken value) -> {
			tempMap.put(key < event.fOffset ? key : key + change, value); 
		});

		AstSyntaxHighlightTokens.setOffsetMap(identifier.getDisplayName(), tempMap);
	}
}
