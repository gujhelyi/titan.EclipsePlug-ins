/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/

package org.eclipse.titan.designer.AST.TTCN3.types;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.Assignment.Assignment_type;
import org.eclipse.titan.designer.AST.FieldSubReference;
import org.eclipse.titan.designer.AST.IReferenceChain;
import org.eclipse.titan.designer.AST.IType;
import org.eclipse.titan.designer.AST.IValue;
import org.eclipse.titan.designer.AST.Identifier;
import org.eclipse.titan.designer.AST.Identifier.Identifier_type;
import org.eclipse.titan.designer.AST.Location;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.Reference.Ref_Type;
import org.eclipse.titan.designer.AST.ReferenceChain;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.Type;
import org.eclipse.titan.designer.AST.TypeCompatibilityInfo;
import org.eclipse.titan.designer.AST.TypeCompatibilityInfo.Chain;
import org.eclipse.titan.designer.AST.TTCN3.Expected_Value_type;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.designer.AST.TTCN3.definitions.FormalParameter;
import org.eclipse.titan.designer.AST.TTCN3.definitions.FormalParameterList;
import org.eclipse.titan.designer.AST.TTCN3.definitions.VisibilityModifier;
import org.eclipse.titan.designer.AST.TTCN3.statements.Assignment_Statement;
import org.eclipse.titan.designer.AST.TTCN3.statements.Return_Statement;
import org.eclipse.titan.designer.AST.TTCN3.statements.StatementBlock;
import org.eclipse.titan.designer.AST.TTCN3.statements.StatementBlock.ReturnStatus_type;
import org.eclipse.titan.designer.AST.TTCN3.templates.ITTCN3Template;
import org.eclipse.titan.designer.AST.TTCN3.templates.SpecificValue_Template;
import org.eclipse.titan.designer.AST.TTCN3.templates.TTCN3Template;
import org.eclipse.titan.designer.AST.TTCN3.values.Referenced_Value;
import org.eclipse.titan.designer.compiler.JavaGenData;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;

/**
 * Represents TTCN3 properties of the OOP extension
 *
 * @author Miklos Magyari
 * */
public final class Property_Type extends Type {
	private static final String INITIALVALUEWITHOUTSETTER = "A property without a setter cannot have an initial value";
	private static final String EMPTYBODY = "An empty property body is not allowed";
	private static final String GETMISSINGRETURN = "Statement block of a property getter should have a `return' statement";

	private static final String MODIFIERFINALABSTRACT = "Property {0} cannot be both abstract and final";
	private static final String ABSTRACTWITHBODY = "Abstract {0} should not have a body";

	private static final String GETTER = "getter";
	private static final String SETTER = "setter";

	/** type of the property */
	private Type myType;

	/** statement block of the property getter, if defined */
	private StatementBlock getterStatementBlock;

	/** statement block of the property setter, if defined */
	private StatementBlock setterStatementBlock;

	/** template definition of the property getter, if defined */
	private TTCN3Template getterTemplate;

	/** template definition of the property setter, if defined */
	private Assignment_Statement setterAssignment;

	/** a template containing the initial value for the property */
	private TTCN3Template initValTemplate;

	/** a fake formal param list with one member to support the 'value' keyword */
	private FormalParameterList fpList;

	private boolean hasGetter;
	private boolean hasSetter;

	/** Indicates if the property's body is empty. */
	private boolean hasBody;

	private boolean isAutoProperty;

	/** Modifiers */
	private boolean isAbstract;
	private boolean isFinal;
	private boolean isDeterministic;
	private boolean isInternal;
	private boolean isGetterAbstract;
	private boolean isSetterAbstract;
	private boolean isGetterFinal;
	private boolean isSetterFinal;
	private boolean isGetterDeterministic;
	private boolean isSetterDeterministic;
	private VisibilityModifier getterModifier;
	private VisibilityModifier setterModifier;
	private Location getterVisibilityLocation;
	private Location setterVisibilityLocation;
	private Location getterModifierLocation;
	private Location setterModifierLocation;

	public Property_Type(Type type, boolean isAbstract, boolean isFinal, boolean isDeterministic, boolean isInternal) {
		this.myType = type;
		this.isAbstract = isAbstract;
		this.isFinal = isFinal;
		this.isDeterministic = isDeterministic;
		this.isInternal = isInternal;

		this.setterStatementBlock = null;
		this.getterStatementBlock = null;
		this.setterAssignment = null;
		this.getterTemplate = null;
		this.initValTemplate = null;
		this.isAutoProperty = true;
		this.hasSetter = false;
		this.hasGetter = false;
		this.hasBody = false;

		// constructing a fake formal parameter list for supporting the 'value' keyword
		FormalParameter valueParam = new FormalParameter(null, Assignment_type.A_PAR_VAL_IN, myType, new Identifier(Identifier_type.ID_TTCN, "value"), null, null);
		List<FormalParameter> paramList = new ArrayList<FormalParameter>();
		paramList.add(valueParam);
		fpList = new FormalParameterList(paramList);
	}

	/**
	 * Sets the statement block for the property setter. If it is {@code null} it generates one.
	 * @param sb Statement block of the property setter
	 * @param template Template of the property setter
	 * @param modifier Visibility
	 * @param isAbstract
	 * @param isFinal
	 * @param isDeterministic
	 * @param visibilityLocation
	 * @param modifierLocation
	 */
	public void setStatementBlockGetter(StatementBlock sb, TTCN3Template template, VisibilityModifier modifier,
			boolean isAbstract, boolean isFinal, boolean isDeterministic,
			Location visibilityLocation, Location modifierLocation) {
		hasGetter = true;
		getterStatementBlock = sb;
		getterModifier = modifier;
		isGetterAbstract = isAbstract;
		isGetterFinal = isFinal;
		isGetterDeterministic = isDeterministic;
		getterVisibilityLocation = visibilityLocation;
		getterModifierLocation = modifierLocation;
		getterTemplate = template;

		if (getterStatementBlock == null && getterTemplate != null) {
			final Return_Statement rs = new Return_Statement(getterTemplate);
			rs.setLocation(getterTemplate.getLocation());
			getterStatementBlock = new StatementBlock();
			getterStatementBlock.addStatement(rs);
		}

		if (getterStatementBlock != null) {
			getterStatementBlock.setIsGetter();
			getterStatementBlock.setOwnerIsProperty();
			getterStatementBlock.setFullNameParent(this);
		}
	}

	/**
	 * Sets the statement block for the property setter. If it is {@code null} it generates one.
	 * @param sb Statement block of the property setter
	 * @param assignment Assignment statement of the property setter
	 * @param modifier Visibility
	 * @param isAbstract
	 * @param isFinal
	 * @param isDeterministic
	 * @param visibilityLocation
	 * @param modifierLocation
	 */
	public void setStatementBlockSetter(StatementBlock sb, Assignment_Statement assignment, VisibilityModifier modifier,
		boolean isAbstract, boolean isFinal, boolean isDeterministic,
		Location visibilityLocation, Location modifierLocation) {
		hasSetter = true;
		setterStatementBlock = sb;
		isSetterAbstract = isAbstract;
		isSetterFinal = isFinal;
		isSetterDeterministic = isDeterministic;
		setterVisibilityLocation = visibilityLocation;
		setterModifierLocation = modifierLocation;
		setterAssignment = assignment;
	
		if (setterStatementBlock == null && setterAssignment != null) {
			setterStatementBlock = new StatementBlock();
			setterStatementBlock.addStatement(setterAssignment);
		}
	
		if (setterStatementBlock != null) {
			setterStatementBlock.setIsSetter();
			setterStatementBlock.setOwnerIsProperty();
			setterStatementBlock.setFullNameParent(this);
		}
	}
	
	@Override
	/** {@inheritDoc} */
	public void check(final CompilationTimeStamp timestamp) {
		if (lastTimeChecked != null && !lastTimeChecked.isLess(timestamp)) {
			return;
		}

		lastTimeChecked = timestamp;
	
		if (hasBody && !hasGetter() && !hasSetter()) {
			getLocation().reportSemanticError(EMPTYBODY);
		}

		checkModifiers();
	
		if (getterStatementBlock != null) {
			getterStatementBlock.check(timestamp);
			if (getterStatementBlock.hasReturn(timestamp) == ReturnStatus_type.RS_NO) {
				getterStatementBlock.getLocation().reportSemanticError(GETMISSINGRETURN);
			}
		}
		if (setterStatementBlock != null) {
			setterStatementBlock.setValueParamList(fpList);
			setterStatementBlock.check(timestamp);
		}
		if (initValTemplate != null) {
			if (hasSetter() == false) {
				initValTemplate.getLocation().reportSemanticError(INITIALVALUEWITHOUTSETTER);
				initValTemplate.setIsErroneous(true);
			}
			initValTemplate.setMyGovernor(myType);
			final IValue value = initValTemplate.getValue();
			if (value != null) {
				value.setMyGovernor(myType);
				myType.checkThisValueRef(timestamp, value);
				myType.checkThisValue(timestamp, value, null,
					new ValueCheckingOptions(Expected_Value_type.EXPECTED_DYNAMIC_VALUE, false, false, true, false, false));
			}
		}
	}

	/**
	 * Semantic check of property getter/setter modifiers
	 */
	public void checkModifiers() {
		if (isAbstract) {
			isGetterAbstract = true;
			isSetterAbstract = true;
		}

		if (isGetterAbstract) {
			if (isGetterFinal) {
				getterModifierLocation.reportSemanticError(MessageFormat.format(MODIFIERFINALABSTRACT, GETTER));
			}
			if (getterStatementBlock != null || getterTemplate != null) {
				final Location getterLoc = getterStatementBlock == null ? getterTemplate.getLocation() : getterStatementBlock.getLocation();
				getterLoc.reportSemanticError(MessageFormat.format(ABSTRACTWITHBODY, GETTER));
			}
		}
		if (isSetterAbstract) {
			if (isSetterFinal) {
				setterModifierLocation.reportSemanticError(MessageFormat.format(MODIFIERFINALABSTRACT, SETTER));
			}
			if (setterStatementBlock != null || setterAssignment != null) {
				final Location setterLoc = setterStatementBlock == null ? setterAssignment.getLocation() : setterStatementBlock.getLocation();
				setterLoc.reportSemanticError(MessageFormat.format(ABSTRACTWITHBODY, SETTER));
			}
		}
	}

	@Override
	public Type_type getTypetype() {
		return Type_type.TYPE_PROPERTY;
	}

	@Override
	public String getTypename() {
		return "property";
	}

	@Override
	public Type_type getTypetypeTtcn3() {
		return myType.getTypetype();
	}

	 @Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		 super.setMyScope(scope);
		 if (getterStatementBlock != null) {
			 getterStatementBlock.setMyScope(scope);
		 }
		 if (setterStatementBlock != null) {
			 setterStatementBlock.setMyScope(scope);
		 }
		 if (initValTemplate != null) {
			 initValTemplate.setMyScope(scope);
		 }
	}

	/** Checks whether the property has a setter definition
	 * @return
	 */
	public boolean hasSetter() {
		return isAutoProperty ? true : hasSetter;
	}

	/** Checks whether the property has a getter definition
	 * @return
	 */
	public boolean hasGetter() {
		return isAutoProperty ? true : hasGetter;
	}
	
	public void setInitValTemplate(TTCN3Template template) {
		initValTemplate = template;
	}

	public void setHasBody() {
		hasBody = true;
	}

	public void setIsAutoProperty(boolean isAutoProperty) {
		this.isAutoProperty = isAutoProperty;
	}

	@Override
	public String getOutlineIcon() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IType getFieldType(CompilationTimeStamp timestamp, Reference reference, int actualSubReference,
			Expected_Value_type expectedIndex, IReferenceChain refChain, boolean interruptIfOptional) {
		return myType;
	}

	@Override
	/** {@inheritDoc} */
	public IType getTypeRefdLast(final CompilationTimeStamp timestamp) {
		final IReferenceChain referenceChain = ReferenceChain.getInstance(IReferenceChain.CIRCULARREFERENCE, true);
		final IType result = getTypeRefdLast(timestamp, referenceChain);
		referenceChain.release();

		return result;
	}

	/**
	 * Returns the type referred last in case of a referred type, or itself
	 * in any other case.
	 *
	 * @param timestamp
	 *                the time stamp of the actual semantic check cycle.
	 * @param referenceChain
	 *                the ReferenceChain used to detect circular references
	 *
	 * @return the actual or the last referred type
	 * */
	public IType getTypeRefdLast(final CompilationTimeStamp timestamp, final IReferenceChain referenceChain) {
		return this.myType;
	}

	@Override
	public boolean checkThisTemplate(CompilationTimeStamp timestamp, ITTCN3Template template, boolean isModified,
			boolean implicitOmit, Assignment lhs) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCompatible(CompilationTimeStamp timestamp, IType otherType, TypeCompatibilityInfo info,
			Chain leftChain, Chain rightChain) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean generatesOwnClass(JavaGenData aData, StringBuilder source) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void generateCode(JavaGenData aData, StringBuilder source) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getGenNameValue(JavaGenData aData, StringBuilder source) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getGenNameTemplate(JavaGenData aData, StringBuilder source) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getGenNameTypeDescriptor(JavaGenData aData, StringBuilder source) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Sets the definition of the getter/setter statement blocks. It should point to a Def_Var instance.
	 * @param definition
	 */
	public void setDefinitions(Definition definition) {
		if (getterStatementBlock != null) {
			getterStatementBlock.setMyDefinition(definition);
		}
		if (setterStatementBlock != null) {
			setterStatementBlock.setMyDefinition(definition);
		}
	}

	public boolean isInternal() {
		return isInternal;
	}

	public boolean isAutoProperty() {
		return isAutoProperty;
	}

	/**
	 * Generates the statement blocks for auto properties.
	 * @param definition Definition of specific property. It should point to a Def_Var instance.
	 */
	public void generateAutoProperty(Definition definition) {
		if (!isAutoProperty) {
			return;
		}

		final VisibilityModifier vm = definition.getVisibilityModifier();

		final Reference valueRef = new Reference(null, Reference.Ref_Type.REF_VALUE);
		final Identifier valueId = new Identifier(Identifier_type.ID_TTCN, "value", null);
		final FieldSubReference valueSubReference = new FieldSubReference(valueId);
		valueRef.addSubReference(valueSubReference);

		final Reference thisRef = new Reference(definition.getIdentifier(), Ref_Type.REF_BASIC);
		final FieldSubReference thisSubReference = new FieldSubReference(definition.getIdentifier());
		thisRef.addSubReference(thisSubReference);

		final TTCN3Template temp = new SpecificValue_Template(new Referenced_Value(thisRef));
		final TTCN3Template tempValue = new SpecificValue_Template(new Referenced_Value(valueRef));
		final Assignment_Statement ass = new Assignment_Statement(thisRef, tempValue);

		setStatementBlockGetter(null, temp, vm, isAbstract, isFinal, isDeterministic, null, null);
		setStatementBlockSetter(null, ass, vm, isAbstract, isFinal, isDeterministic, null, null);
	}
}
