/******************************************************************************
 * Copyright (c) 2000-2021 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
package org.eclipse.titan.designer.AST.TTCN3.types;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import org.eclipse.titan.designer.AST.ASTNode;
import org.eclipse.titan.designer.AST.ASTVisitor;
import org.eclipse.titan.designer.AST.Assignment;
import org.eclipse.titan.designer.AST.ILocateableNode;
import org.eclipse.titan.designer.AST.Location;
import org.eclipse.titan.designer.AST.Reference;
import org.eclipse.titan.designer.AST.Scope;
import org.eclipse.titan.designer.AST.Type;
import org.eclipse.titan.designer.AST.TTCN3.IIncrementallyUpdateable;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Function;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Def_Type;
import org.eclipse.titan.designer.AST.TTCN3.definitions.Definition;
import org.eclipse.titan.designer.parsers.CompilationTimeStamp;
import org.eclipse.titan.designer.parsers.ttcn3parser.ReParseException;
import org.eclipse.titan.designer.parsers.ttcn3parser.TTCN3ReparseUpdater;

/**
 * @author Miklos Magyari
 * */
public final class ClassTypeReferenceList extends ASTNode implements ILocateableNode, IIncrementallyUpdateable {
	private final String ONLYONNONTRAIT = "A class can only extend one non-trait class";
	private final String TRAITEXTENDSTRAIT = "A trait class can only extend trait classes";
	private final String FINALCANNOTBEEXTENDED = "A final class cannot be extended";
	private final String EXTERNALEXTEND = "An external class cannot be extended by a non-external class";
	private final String DUPLICATECOMPONENTREFERENCEFIRST = "Duplicate reference to class `{0}'' was first declared here";
	private final String DUPLICATECOMPONENTREFERENCEREPEATED = "Duplicate reference to class `{0}'' was declared here again";
	private final String CLASSCANNOTEXTENDITSELF = "A class cannot extend itself neither directly nor indirectly";
	private final String ABSTRACTMETHODUNIMPLEMENTED = "Class must implement abstract method `{0}'' inherited from class `{1}''"; 
	private final String INCOMPATIBLERUNSON = "Class `{0}'' is not 'runs on' compatible with parent `{1}''";
	private final String INCOMPATIBLEMTC= "Class `{0}'' is not 'mtc' compatible with parent `{1}''";
	private final String INCOMPATIBLESYSTEM = "Class `{0}'' is not 'system' compatible with parent `{1}''";
	
	private Class_Type myClass;
	
	private final List<Reference> classReferences;
	private Location location;
	private Map<ClassTypeBody, Reference> classTypeBodies;
	private List<ClassTypeBody> orderedClassTypeBodies;
	private CompilationTimeStamp lastCompilationTimeStamp;
	
	public ClassTypeReferenceList() {
		classReferences = new CopyOnWriteArrayList<Reference>();
	}
	
	public void addReference(final Reference reference) {
		classReferences.add(reference);
		reference.setFullNameParent(this);
	}
	
	public List<Reference> getReferenceList() {
		return classReferences;
	}
	
	public void setParentClass(Class_Type parentClass) {
		this.myClass = parentClass;		
	}
	
	public List<ClassTypeBody> getClassBodies(CompilationTimeStamp timestamp) {
		if (timestamp != null) {
			checkUniqueness(timestamp);
		}
		return orderedClassTypeBodies;
	}
	
	@Override
	public Location getLocation() {
		return location;
	}
	
	@Override
	public void setLocation(Location location) {
		this.location = location;
	}
	
	@Override
	/** {@inheritDoc} */
	public void setMyScope(final Scope scope) {
		for (final Reference reference : classReferences) {
			reference.setMyScope(scope);
		}
	}
	
	public void check(final CompilationTimeStamp timestamp) {
		if (lastCompilationTimeStamp != null && !lastCompilationTimeStamp.isLess(timestamp)) {
			return;
		}

		lastCompilationTimeStamp = timestamp;
		
		int nrNonTraitExtends = 0;
		for (final Reference classRef : classReferences) {
			final Assignment refdAss = classRef.getRefdAssignment(timestamp, false);
			if (refdAss instanceof Def_Type) {
				final Def_Type deftype = (Def_Type)refdAss;
				final Type type  = deftype.getType(timestamp);
				if (type instanceof Class_Type) {					
					final Class_Type extClass = (Class_Type)type;
					
					final Component_Type extRunsOnType = extClass.getRunsOnType(timestamp);
					final Component_Type myRunsOnType = myClass.getRunsOnType(timestamp);
					if (myRunsOnType != null && !myRunsOnType.isCompatible(timestamp, extRunsOnType, null, null, null)) {
						myClass.getRunsOnRef().getLocation().reportSemanticError(
							MessageFormat.format(INCOMPATIBLERUNSON, myClass.getFullName(), extClass.getFullName()));
					}
					
					final Component_Type extMtcType = extClass.getMtcType(timestamp);
					final Component_Type myMtcType = myClass.getMtcType(timestamp);
					if (myMtcType != null && !myMtcType.isCompatible(timestamp, extMtcType, null, null, null)) {
						myClass.getMtcRef().getLocation().reportSemanticError(
							MessageFormat.format(INCOMPATIBLEMTC, myClass.getFullName(), extClass.getFullName()));
					}
					
					final Component_Type extSystemType = extClass.getSystemType(timestamp);
					final Component_Type mySystemType = myClass.getSystemType(timestamp);
					if (mySystemType != null && !mySystemType.isCompatible(timestamp, extSystemType, null, null, null)) {
						myClass.getSystemRef().getLocation().reportSemanticError(
							MessageFormat.format(INCOMPATIBLESYSTEM, myClass.getFullName(), extClass.getFullName()));
					}
					
					if (extClass == myClass) {
						classRef.getLocation().reportSemanticError(CLASSCANNOTEXTENDITSELF);
					}
					if (extClass.isFinal()) {
						classRef.getLocation().reportSemanticError(FINALCANNOTBEEXTENDED);
						myClass.setIsErroneous(true);
					}
					if (! extClass.isTrait()) {
						nrNonTraitExtends++;
						if (myClass.isTrait()) {
							classRef.getLocation().reportSemanticError(TRAITEXTENDSTRAIT);
							myClass.setIsErroneous(true);
						} else {
							if (nrNonTraitExtends > 1) {
								classRef.getLocation().reportSemanticError(ONLYONNONTRAIT);
								myClass.setIsErroneous(true);
							}
						}
					}
					if (extClass.isExternal() && ! myClass.isExternal()) {
						classRef.getLocation().reportSemanticError(EXTERNALEXTEND);
					}
					checkAbstractOverride(timestamp, extClass, myClass);
				}
			}
		}
		
		checkSelfReference(timestamp, myClass);
		checkUniqueness(timestamp);

		for (final ClassTypeBody body : orderedClassTypeBodies) {
			body.check(timestamp);
		}
	}
	
	/**
	 * Ensures that abstract methods inherited from abstract parent classes are implemented 
	 * @param timestamp
	 * @param extClass
	 * @param parentClass
	 */
	private void checkAbstractOverride(CompilationTimeStamp timestamp, Class_Type extClass, Class_Type parentClass) {
		if (parentClass.isAbstract() || parentClass.isTrait()) {
			return;
		}
		if (extClass.isAbstract()) {
			for (Definition def : extClass.getClassBody().getDefinitions()) {
				if (def instanceof Def_Function) {
					final Def_Function funcDef = (Def_Function)def;
					if (funcDef.isAbstract()) {
						if (! parentClass.getClassBody().hasAssignmentWithId(timestamp, funcDef.getIdentifier())) {
							parentClass.getClassBody().getIdentifier().getLocation().reportSemanticError(
									MessageFormat.format(ABSTRACTMETHODUNIMPLEMENTED, funcDef.getIdentifier().getName(), 
											extClass.getFullName()));
						}
					}
				}
			}
		}
	}
	
	/**
	 * Checks the uniqueness of the extensions, and also builds a hashmap of
	 * them to speed up further searches.
	 *
	 * @param timestamp the timestamp of the actual semantic check cycle
	 * */
	private void checkUniqueness(final CompilationTimeStamp timestamp) {
		if (orderedClassTypeBodies == null) {
			classTypeBodies = new HashMap<ClassTypeBody, Reference>(classReferences.size());
			orderedClassTypeBodies = new ArrayList<ClassTypeBody>(classReferences.size());
		}

		classTypeBodies.clear();
		orderedClassTypeBodies.clear();

		for (final Reference reference : classReferences) {
			final Class_Type classType = reference.chkClassReference(timestamp);
			if (classType != null) {
				final ClassTypeBody classTypeBody = classType.getClassBody();
				if (classTypeBody != null) {
					if (classTypeBodies.containsKey(classTypeBody)) {
						classTypeBodies.get(classTypeBody).getId().getLocation().reportSingularSemanticError(
								MessageFormat.format(DUPLICATECOMPONENTREFERENCEFIRST, classTypeBody.getIdentifier()
										.getDisplayName()));
						reference.getLocation().reportSemanticError(
								MessageFormat.format(DUPLICATECOMPONENTREFERENCEREPEATED, reference.getDisplayName()));
					} else {
						classTypeBodies.put(classTypeBody, reference);
						orderedClassTypeBodies.add(classTypeBody);
					}
				}
			}
		}
	}
	
	/**
	 * Checks if a class has 'extends' references.
	 * @return true if the class has parents other than 'object', false otherwise.
	 */
	public boolean hasExtendsReferences() {
		return classReferences.size() > 0;
	}
	
	/**
	 * Checks if the list of class references contains a given class 
	 * 
	 * @param timestamp
	 * @param refdClass
	 * @return
	 */
	public boolean checkExtendsReference(final CompilationTimeStamp timestamp, Class_Type refdClass) {
		if (refdClass == null) {
			return false;
		}
		for (Reference ref : classReferences) {
			final Assignment refdAss = ref.getRefdAssignment(timestamp, false);
			if (refdAss == null) {
				return false;
			}
			if (refdAss instanceof Def_Type) {
				final Def_Type deftype = (Def_Type)refdAss;
				final Type type  = deftype.getType(timestamp);
				if (type instanceof Class_Type) {
					if ((Class_Type)type == refdClass) {
						ref.getLocation().reportSemanticError(CLASSCANNOTEXTENDITSELF);
						return true;
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * Walks the class extension hierarchy and checks if classes up in the 'extends' chain extend the given class
	 *  
	 * @param timestamp
	 * @param refdClass
	 * @return
	 */
	public boolean checkSelfReference(final CompilationTimeStamp timestamp, Class_Type refdClass) {
		if (refdClass == null) {
			return false;
		}
		for (Reference ref : classReferences) {
			final Assignment refdAss = ref.getRefdAssignment(timestamp, false);
			if (refdAss == null) {
				return false;
			}
			if (refdAss instanceof Def_Type) {
				final Def_Type deftype = (Def_Type)refdAss;
				final Type type  = deftype.getType(timestamp);
				if (type instanceof Class_Type) {
					final Class_Type ctype = (Class_Type)type;
					if (refdClass == ctype) {
						ref.getLocation().reportSemanticError(CLASSCANNOTEXTENDITSELF);
						return true;
					}					
					if (checkExtendsReference(timestamp, refdClass)) {
						return true;
					}
					if (ctype.getClassBody().getExtendsReferences().checkSelfReference(timestamp, refdClass)) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	@Override
	/** {@inheritDoc} */
	public void updateSyntax(final TTCN3ReparseUpdater reparser, final boolean isDamaged) throws ReParseException {
		if (isDamaged) {
			throw new ReParseException();
		}

		for (final Reference reference : classReferences) {
			reference.updateSyntax(reparser, false);
			reparser.updateLocation(reference.getLocation());
		}
	}

	@Override
	/** {@inheritDoc} */
	protected boolean memberAccept(final ASTVisitor v) {
		if (classReferences != null) {
			for (final Reference ref : classReferences) {
				if (!ref.accept(v)) {
					return false;
				}
			}
		}
		return true;
	}
	
}
